# Data Type

Every programming language supports basic data types, using these types we can create complex types and **Data Structures**

These types are classified into two groups:

1. **Primitive (Primary)**: Builtin types
2. **Non-Primitive (Derived)**: Sum type, Product type

![Data Types](./assets/data_types.jpg)

---

## Primitive Data Types

The types, **already defined** in language

Primitive data types are those data types which are `already defined` in the `programming languages` also called **primary** or **built-in** data types

1. **Boolean**
    - **bool**: Simple boolean $\implies$ 1 bit + 7 bit padding (`1 byte`)
2. **Character**
    - **char**: Ascii character $\implies$ 8 bit (`1 byte`)
    - **wchar_t**: Unicode character $\implies$ dynamic size
    - **char16_t**: UTF-16 character $\implies$ 16 bit (`2 byte`)
    - **char32_t**: UTF-32 character $\implies$ 32 bit (`4 byte`)
3. **Integer**
    - **byte**: One-Byte integer $\implies$ 8 bit (`1 byte`)
    - **short**: Short integer $\implies$ 16 bit (`2 byte`)
    - **int**: Simple integer $\implies$ 32 bit (`4 byte`)
    - **long**: Long integer $\implies$ 64 bit (`8 byte`)
4. **Floating-Point**
    - **float**: Simple Float $\implies$ 32 bit (`4 byte`)
    - **double**: Double Float $\implies$ 64 bit (`8 byte`)

---

### Example

```cpp
bool a = false;                 // 1 byte

char b = 'x';                   // 1 byte

short c = 0b11101110;           // 2 byte
int d = 410000101111;           // 4 byte
long e = 11e20;                 // 8 byte

float f = 141.131;              // 4 byte
double g = 141411333.11441;     // 8 byte
```

---

## Non-Primitive Data Types

The types, **we define** in language

These data types, defined by user or programmer, they will created using **Primitive Data Types**

1. **enum**: `2 byte`
2. **union**: `max{elements size}`
3. **struct**: `sum{elements size} + padding`
4. **array**: `elements size * size + padding`
5. **string**: `(string length + 1) * character size + padding`
6. **pointer**: `8 byte | 4 byte` (depends on CPU architecture)
7. **function**: `8 byte | 4 byte` (depends on CPU architecture) (Pointer)

---

### Example

```cpp
enum A {X, Y, Z};
enum A a = A.X;                 // 2 byte

union B {int x; double y;};
union B b;                      // max{4,8} = 8 byte
b.x = 43;

struct C {int x; double y;};
struct C c;                     // sum{4,8} + padding = 12 + 4 = 16 byte
c.x = 1551;
c.y = 3131.11;

float d[1000];                  // 4 * 1000 = 4000 byte
d[0] = 4.4;
d[5] = 11.11;

char* s = "abcd";               // 1 * (4+1) = 5 byte
// a, b, c, d, \0

void* e = malloc(1024);         // 8 byte (depends on CPU architecture)

int(*f)(float,int) =            // 8 byte (like pointer)
    (float x, int y) => {
        return y;
    };
```

---

### Memory Padding

Depends on the CPU architecture, every time it wants to **LOAD** data from the memory, it will load a fixed size of data into **CPU** registers

If **CPU Register** was:

1. **32 bit**: Every data type should have a size with **multiple of 32 bit**
2. **64 bit**: Every data type should have a size with **multiple of 64 bit**

So the paddings will add to **Data Types**, before saving them

For example:

```cpp
// CPU: 32 bit
short a = 5;        // 16 bit (data) + 16 bit (padding) = 32 bit

// CPU: 64 bit
short a = 5;        // 16 bit (data) + 48 bit (padding) = 64 bit
```

---

### Algebraic Data Types (ADT)

In **Type Theory**, exists two important types:

1. **Sum Type**: Union, Or
2. **Product Type**: Struct, Class, Interface, And
3. **Recursive Type**: Recursive

These types exists in **TypeScript**:

```ts
// Sum type
type A = String | Number;

// Product type
type B = {
    x: String;
    y: Number;
};

// Recursive type
type C = [Number, C];
```

Using these types we can create powerful and **safe** data structures

---

#### Product Type

We can use **Product Type** in any programming language, this type implemented in languages in two forms:

1. **Struct** (Procedural)
2. **Class** (Object Oriented)

| Struct           | Class                                   |
| ---------------- | --------------------------------------- |
| Only `Data`      | `Behavior` and `Data`                   |
| `Simple` Create  | `Explicit` initiation (`new`, `malloc`) |
| `Value` types    | `Pointer` type                          |
| `No oop` feature | `Polymorphism`, `Inheritance`           |

---
