# Linked List

**Linked Stack** is a type of **Stack** implemented with `singly linked list`

![Linked Stack](../assets/linked_stack.png)

---

## Concepts

There are many important concepts in **Linked Stack**

---

### Top

-   The `top` node is **Head** in `linked list`

---

## IsEmpty

```cpp
struct Node* head = ...;

bool isEmpty() {
    return head->next == NULL;
}
```

---

## IsFull

```cpp
struct Node* head = ...;

bool isFull() {
    return false;
}
```

---

## Push

```cpp
struct Node* head = ...;

void push(int value) {
    if (isFull()) {
        throw "Stack overflow";
    }

    struct Node* node = malloc(sizeof(struct Node));
    node->next = head->next;
    node->data = value;

    head->next = node;
}
```

---

## Pop

```cpp
struct Node* head = ...;

int pop() {
    if (isEmpty()) {
        throw "Stack underflow";
    }

    struct Node* node = head->next;
    head->next = node->next;

    int result = node->data;
    free(node);
    return result;
}
```

---

## Peek

```cpp
struct Node* head = ...;

int peek() {
    if (isEmpty()) {
        throw "Stack underflow";
    }

    return head->next->data;
}
```

---
