# Array Stack

**Array Stack** is a type of **Stack** implemented with `static array`

![Stack Array](../assets/array_stack.png)

---

## Concepts

There are many important concepts in **Array Stack**

---

### Top

We must save a variable named **Top**, point to the max index of stack:

1. **array list**
2. **top** variable

---

### Two stack

We can implement two stacks in one array using:

1. Two **top** pointers
2. **reverse** stacks

This type of stack is fully optimized so there isn't any block that may never use

![Two](../assets/array_stack_two.jpg)

---

### Many stack

We can implement more than two stacks in one array using:

1. **n** top pointers
2. **n+1** bottom pointers

In general format we can create **m** stacks in array with size **n** so:

$$
\begin{aligned}
    & \texttt{Size of each stack}: \lfloor \frac{n}{m} \rfloor
    \\
    \\
    & \texttt{Top variables}: top[1..m]
    \\
    \\
    & \texttt{Bottom variables}: bottom[1..(m+1)]
    \\
    \\
    & t[i] = b[i] = (i - 1) * \lfloor \frac{n}{m} \rfloor
\end{aligned}
$$

![Many](../assets/array_stack_many.png)

---

## IsEmpty

```cpp
int top = 5;
int array[100] = {10,21,33,9,11};

bool isEmpty() {
    return top == 1;
}
```

---

## IsFull

```cpp
int top = 5;
int array[100] = {10,21,33,9,11};

bool isFull() {
    return top == 100;
}
```

---

## Push

```cpp
int top = 5;
int array[100] = {10,21,33,9,11};

void push(int value) {
    if (isFull()) {
        throw "Stack overflow";
    }

    array[top] = value;
    top++;
}
```

---

## Pop

```cpp
int top = 5;
int array[100] = {10,21,33,9,11};

int pop() {
    if (isEmpty()) {
        throw "Stack underflow";
    }

    top--;
    return array[top];
}
```

---

## Peek

```cpp
int top = 5;
int array[100] = {10,21,33,9,11};

int peek() {
    if (isEmpty()) {
        throw "Stack underflow";
    }

    return array[top-1];
}
```

---
