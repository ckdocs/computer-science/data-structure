# Priority Stack

**Priority Stack** is a type of **Stack** that items will `pop or push` by their `priority order`

---

## Concepts

There are many important concepts in **Priority Stack**

---

### Implementation

There are many ways to implement a `priority stack`:

-   **Insertion Sort**: Ordering when `push`
-   **Selection Sort**: Ordering when `pop`
-   **Binary Search Tree**: Ordering using `BST` or `AVL` or `Red-Black`
-   **Binary Heap**: Ordering using `Binary Heap`

---
