# Stack

Is a **ADT (Abstract Data Type)** works in **LIFO (Last In First Out)** or **FILO (First In Last Out)**

![Stack](../assets/stack.png)

There are many concepts in **Stacks**:

![Concepts](../assets/stack_concepts.png)

1. **Top**: position of **new** items added to list
2. **Bottom**: position of **old** items added to list

---

## Top

The top of stack:

1. **End** of array list (**End** - **Index MAX**)
2. **Head** of linked list (**Start**)
3. By **adding** elements top will **increase**

---

## Bottom

The bottom of stack:

1. **Start** of array list (**Start** - **Index 0**)
2. **Tail** of linked list (**End**)

If we have multiple stacks in one **List** we should save their **Bottoms**, but if we use only one, the index **0** is the bottom of stack

---

## Orders

![Orders](../assets/stack_orders.png)

---
