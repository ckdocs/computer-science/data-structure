# Associative Map

**Associative Map** or **Associative Array** or **Multi Map** is a type of **Map** which store **Key-Value Pairs** in **Rows** as a `table`

![Associative Map](../assets/associative_map.png)

---

## Concepts

There are many important concepts in **Associative Map**

---

### Searching

-   For **Searching** operation we must search in `all keys` with complexity of $O(n)$

---

## Iterate

```cpp
int top = 3;
int map[10][2] = {
    {11, 2412},
    {45, 7331},
    {104, 1311},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

void iterate() {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        print(map[cursor]);
    }
}
```

-   **Tip**: We can implement higher level operations using map iterations in $O(n)$:
    -   **Contains**: Check value exists in map
    -   **KeyOf**: Get key of a value

---

## Search

-   **Iterate** all keys and search

```cpp
int top = 3;
int map[10][2] = {
    {11, 2412},
    {45, 7331},
    {104, 1311},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

int search(int key) {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        if (map[cursor][0] == key) {
            return map[cursor][1];
        }
    }

    return -1;
}
```

---

## Add

-   **Set** the `top` value to **Key-Value**
-   **Increase** array top

```cpp
int top = 3;
int map[10][2] = {
    {11, 2412},
    {45, 7331},
    {104, 1311},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

void add(int key, int value) {
    map[top] = {key, value};

    top++;
}
```

---

## Remove

-   **Search** for `key`, if found:
    -   **Decrease** array top
    -   **Set** the `index` value to **0-0**
    -   **Left Shift** the `index`

```cpp
int top = 3;
int map[10][2] = {
    {11, 2412},
    {45, 7331},
    {104, 1311},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

void remove(int key) {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        if (map[cursor][0] == key) {
            top--;

            map[cursor] = {0, 0};

            leftShift(map[cursor]);
        }
    }
}
```

---
