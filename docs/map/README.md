# Map

**Map** or **Dictionary** is a structure for saving **Key-Value Pairs** and `searching` by **Unique Keys**

![Map](../assets/map.png)

---

## Orders

![Orders](../assets/map_orders.png)

---
