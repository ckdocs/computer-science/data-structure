# Tree Map

**Tree Map** or **Search Tree Map** is a type of **Map** which use a **Binary Search Tree** in it's core for finding `key-value` pairs

![Tree Map](../assets/tree_map.png)

---

## Concepts

There are many important concepts in **Tree Map**

---

### Key-Value

-   In **Tree Map** structures, we store a `key-value` pair in each node

---

## Iterate

-   **Traverse** tree `DFS` or `BFS`

```cpp
struct BST* bst = ...;

void iterate() {
    return bst.inOrderDFS();
}
```

---

## Search

```cpp
struct BST* bst = ...;

int search(int key) {
    return bst.search(key);
}
```

---

## Add

```cpp
struct BST* bst = ...;

void add(int key, int value) {
    bst.add(key, value);
}
```

---

## Remove

```cpp
struct BST* bst = ...;

void remove(int key) {
    bst.remove(key);
}
```

---
