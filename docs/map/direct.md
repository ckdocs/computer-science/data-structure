# Direct Map

**Direct Map** or **Direct Address Table** is a type of **Map** which keys are **Array Index**

![Direct Map](../assets/direct_map.jpg)

We use **Direct Maps** when:

1. the keys are **small integers**
2. the number of keys is **not too large**
3. no two data have the **same key**

---

## Concepts

There are many important concepts in **Direct Map**

---

### Searching

-   For **Searching** operation we must search in `get by key` with complexity of $O(1)$
-   **Memory Loss**: Key's domain may be large, so we need to allocate a **Large Map** with a lot of empty elements
    -   For fixing this problem we use **Hash Map** to map the key's domain into smaller domain

---

## Iterate

-   **Iterate** through the `map size`
    -   If **Not NULL** print value

```cpp
int map[10] = {0,2,0,0,1,0,0,5,0,0};

void iterate() {
    for (int cursor = 0 ; cursor < 10 ; cursor++) {
        if (map[cursor] != 0) {
            print(map[cursor]);
        }
    }
}
```

---

## Search

```cpp
int map[10] = {0,2,0,0,1,0,0,5,0,0};

int search(int key) {
    return map[key];
}
```

---

## Add

```cpp
int map[10] = {0,2,0,0,1,0,0,5,0,0};

void add(int key, int value) {
    map[key] = value;
}
```

---

## Remove

```cpp
int map[10] = {0,2,0,0,1,0,0,5,0,0};

void remove(int key) {
    map[key] = 0;
}
```

---
