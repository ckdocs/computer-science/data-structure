# Open-Addressed Hash Map

**Open-Addressed Hash Map** is a type of **Hash Map** which store **Collisioned Keys** in the `next free` element

![Open-Addressed Hash Map](../../assets/open_addressed_hash_map.png)

-   **Tip**: This type of hash map can store **Limited** amount of data in size of hash table:
    -   **n <= m**
-   **Tip**: The new elements will add at the **First Empty Element** with **O(1)**

---

## Concepts

Finding the **Next Empty Element** in hash set is a new problem, we must cover all **Elements** of hash map in order to use efficient from memory, there are three methods **Covering All Elements** to find the next:

1. **Linear Probing**: next element is **next index**
2. **Quadratic Probing**: next element is computed from a **quadratic** with **special coefficients**
3. **Double Hashing**: next element is computed from **two special hash function**

![Probing](../../assets/open_addressed_hash_map_probing.png)

---

### Linear Probing

We find the **next empty** element **iteratively** using **index increment**

-   **Tip**: Linear probing has problem of **Primary Clustering**

$$
\begin{aligned}
    & h^{'}(k, i) = (h(k) + i) \;mod\; m
    \\
    & i = 0,1,2,\dots,n
\end{aligned}
$$

---

#### Primary Clustering

Problem of centralizing values in some points, occurs in **Linear Probing Method** to find next empty element

This centeralization will reduce the performance of hash table by **iteration increasing** at cluster points

![Primary Clustering](../../assets/open_addressed_hash_map_primary_clustering.png)

---

### Quadratic Probing

We use **special coefficients** to find next element **irregularly**

-   **Tip**: Quadratic probing has problem of **Secondary Clustering**
-   **Tip**: We can not use any **c** values because of **Completeness**

$$
\begin{aligned}
    & h^{'}(k, i) = (h(k) + c_1.i + c_2.i^2) \;mod\; m
    \\
    & i = 0,1,2,\dots,n
\end{aligned}
$$

-   **Example**: $c_1 = c_2 = \frac{1}{2}$ is **complete**

---

#### Secondary Clustering

Problem of centralizing values in some points, occurs in **Quadratic Probing Method** to find next empty element

This centeralization will reduce the performance of hash table by **iteration increasing** at cluster points

![Secondary Clustering](../../assets/open_addressed_hash_map_secondary_clustering.png)

---

### Double Hashing

We use **special hash function** to find next element **irregularly**

-   **Tip**: We can not use any **hash function** values because of **Completeness**
-   **Tip**: $m$ and $h_2(k)$ must be **prime** to each other
    -   $m$ must be **prime**
    -   $h_2(k)$ must be **less than m**

$$
\begin{aligned}
    & h^{'}(k, i) = (h_1(k) + h_2(k).i) \;mod\; m
    \\
    & i = 0,1,2,\dots,n
\end{aligned}
$$

---

## Search

-   **Iterate** through the `size`
    -   **Generate** hash by `key, cursor`
    -   If key **Found**:
        -   Return `value`

```cpp
int map[10][2] = {
    {0, 0},
    {351, 2112},
    {0, 0},
    {0, 0},
    {21, 1001},
    {97, 6123},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

int search(int key) {
    for (int cursor = 0 ; cursor < 10 ; cursor++) {
        int index = hash(key, cursor);

        // Not found
        if (map[index][0] == 0) {
            return -1;
        }

        // Found
        if (map[index][0] == key) {
            return map[index][1];
        }
    }

    return -1;
}
```

---

## Add

-   **Iterate** through the `size`
    -   **Generate** hash by `key, cursor`
    -   If `empty` or `deleted` element **Found**:
        -   **Fill** `key-value` pair

```cpp
int map[10][2] = {
    {0, 0},
    {351, 2112},
    {0, 0},
    {0, 0},
    {21, 1001},
    {97, 6123},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

void add(int key, int value) {
    for (int cursor = 0 ; cursor < 10 ; cursor++) {
        int index = hash(key, cursor);

        // Empty or Deleted element found
        if (map[index][0] == 0 || map[index][0] == -1) {
            map[index] = {key, value};
            return;
        }
    }

    throw "Map overflow";
}
```

---

## Remove

-   **Iterate** through the `size`
    -   **Generate** hash by `key, cursor`
    -   If key **Found**:
        -   Set `pair state` as **Deleted** (`-1`)

```cpp
int map[10][2] = {
    {0, 0},
    {351, 2112},
    {0, 0},
    {0, 0},
    {21, 1001},
    {97, 6123},
    {0, 0},
    {0, 0},
    {0, 0},
    {0, 0}
};

void remove(int key) {
    for (int cursor = 0 ; cursor < 10 ; cursor++) {
        int index = hash(key, cursor);

        // Not found
        if (map[index][0] == 0) {
            break;
        }

        // Found
        if (map[index][0] == key) {
            map[index] = {-1, -1};
            return;
        }
    }

    throw "Map underflow";
}
```

-   **Tip**: We shouldn't clear key to default, we must set it as **Deleted State** in order to iteration for finding an element don't break (**Destroyed Map**)

---
