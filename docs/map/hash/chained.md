# Chained Hash Map

**Chained Hash Map** is a type of **Hash Map** which store **Collisioned Keys** in a `linked list` of `key-value pairs`

![Chained Hash Map](../../assets/chained_hash_map.png)

-   **Tip**: This type of hash map can store **Unlimited** amount of data:
    -   **m**, **n** are not related
-   **Tip**: The new elements will add at the **First Of Linked List** with **O(1)**

---

## Search

-   **Find** linked list by `hash`
-   **Iterate** list to find `key-value` pair

```cpp
struct LinkedList* map[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

int search(int key) {
    int index = hash(key);
    struct LinkedList* list = map[index];
    struct LinkedNode* node = list->head;

    // Iterate linked list to find key-value pair
    while(node != NULL && node->key != key) {
        node = node->next;
    }

    // Not found
    if (node == NULL) {
        return -1;
    }

    return node->value;
}
```

---

## Add

-   **Find** linked list by `hash`
-   **Add** `key-value` pair to list

```cpp
struct LinkedList* map[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

int add(int key, int value) {
    int index = hash(key);
    struct LinkedList* list = map[index];

    // Add key-value pair to list
    list->add(linkednode_new(key, value));
}
```

---

## Remove

-   **Find** linked list by `hash`
-   **Iterate** list to find `key-value` pair index
-   **Remove** item at index

```cpp
struct LinkedList* map[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

int remove(int key) {
    int index = hash(key);
    struct LinkedList* list = map[index];

    // Iterate linked list to find key-value pair
    while(node->next != NULL && node->next->key != key) {
        node = node->next;
    }

    // Not found
    if (node->next == NULL) {
        return -1;
    }

    struct LinkedNode* old = node->next;
    node->next = node->next->next;
    free(old);
}
```

---
