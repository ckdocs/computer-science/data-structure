# Hash Map

**Hash Map** or **Hash Table** is another structure of saving **Maps** with **Fast Access** and **Lower Space**

In this structure **Hash(Keys)** are our **Values Address**, so we use them to find the value **directly**

![Hash Map](../../assets/hash_map.png)

---

## Load factor

Is a number tells us **How** hash map is **Full**:

$$
\begin{aligned}
    & \texttt{Load factor}: \alpha = \frac{n}{m}
    \\
    \\
    & n: \texttt{Our data pairs}
    \\
    & m: \texttt{Hash table size}
\end{aligned}
$$

---

## Growth factor

For **Performance Optimization** of dynamic maps, when **Load Factor** comes greater than a number:

-   **Increase** map size by a `growth factor` or `prime number`
    -   **Tip**: It's better to use **Prime Numbers** as size of hash map
-   **Recompute** hashes and `replace` items to `new places` (`better distribution`)

---

## Collision

Occurs when **Multiple Keys** have **Same Hash** values, for solving this problem we have two solutions:

1. **Chained**: store collision items in **Linked List** at each element of hash map
2. **Open-Addressed**: store collision items in the **Next Free** element of hash map

---

## Probabilties

-   **Tip**: Probability of `two keys collision` is:

$$
\begin{aligned}
    & P(Collision) = \frac{1}{m}
\end{aligned}
$$

-   **Tip**: Probability of an `element to be full` is:

$$
\begin{aligned}
    & P(Full) = \alpha = \frac{n}{m}
\end{aligned}
$$

---
