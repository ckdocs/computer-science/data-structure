# Data Operation

The main goal of **Data Structures** is to simplify the operations **Performance**

---

## Concepts

Before the start, we must know about the important concepts in **Data Structure**:

1. **Element (Data)**: each data object, stored in data structure
2. **Identifier (Index)**: position of each data object

![Element Identifier](./assets/element_identifier.gif)

---

### Element (Item)

Data Structures, are the structure to save **Data Object** or **Data Element** or **Data Item**

---

### Identifier (Index)

Every Data Element has a position in Data Structure the **Identifier** of this position used to simply find the Element in Data Structure

---

## Operations

These operations categorize in many groups:

1. **Get**
2. **Set**
3. **Traverse**
4. **Search**
5. **Insert**
6. **Delete**
7. etc

The advance operations will discuss in **Algorithm Tutorial**

---

### Get (Value)

The operation of **Getting Element By Identifier**

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

int get(int index) {
    return array[index];
}
```

---

### Set (Update)

The operation of **Setting new Element To Identifier Element**

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void set(int index, int value) {
    array[index] = value;
}
```

---

### Traverse (Iterate)

The operation of **Traversing all Elements**

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void traverse() {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        print(array[cursor]);
    }
}
```

---

### Search (Find)

The operation of **Finding Identifier of an Element**

There are two main sub-operations:

1. **contains**: checks element exists in data structure
2. **indexOf**: finds the index of element in data structure

---

#### containes

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

bool contains(int value) {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        if (array[cursor] == value) {
            return true;
        }
    }

    return false;
}
```

#### indexOf

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

bool indexOf(int value) {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        if (array[cursor] == value) {
            return cursor;
        }
    }

    return -1;
}
```

---

### Insert (Add)

The operation of **Adding a new Element**

There are three main sub-operations:

1. **add**: adds element to specific position of data structure
2. **push**: adds element to **top** of data structure
3. **unshift**: adds element to **bottom** of data structure

![Shift Pop](./assets/shift_pop.svg)

---

#### add

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void add(int index, int value) {
    rightShift(array[index]);
    array[index] = value;
    top++;
}
```

#### push (top)

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void push(int value) {
    array[top] = value;
    top++;
}
```

#### unshift (bottom)

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void unshift(int value) {
    rightShift(array[0]);
    array[0] = value;
    top++;
}
```

---

### Delete (Remove)

The operation of **Removing an Element**

There are three main sub-operations:

1. **remove**: removes element from specific position of data structure
2. **pop**: removes element from **top** of data structure
3. **shift**: removes element from **bottom** of data structure

![Shift Pop](./assets/shift_pop.svg)

---

#### remove

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void remove(int index) {
    top--;
    array[index] = 0;
    leftShift(array[index]);
}
```

#### pop (top)

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void pop() {
    top--;
    array[top] = 0;
}
```

#### shift (bottom)

```cpp
int top = 5;
int array[1000] = {1,2,3,4,5,0,0,...};

void shift() {
    top--;
    array[0] = 0;
    leftShift(array[0]);
}
```

---
