# XOR Linked List

**XOR Linked List** is a **Memory Efficient** version of **Doubly Linked List** using **One Address Field** and **XOR** operator:

1. $X \oplus 0 = X$
2. $X \oplus X = 0$
3. $X \oplus Y = Y \oplus X$
4. $(X \oplus Y) \oplus Z = X \oplus (Y \oplus Z)$

In this type of linked list:

1. Save **XOR** address of **Next** and **Previous** node
2. Use **Two Pointer** pointing to **Head** and **Next**

![XOR Linked List](../assets/xor_linked_list.png)

Now for move operations we can do this:

1. **Move forward**: $D = Address(Next) \oplus Item$
2. **Move backward**: $A = Address(Item) \oplus Next$

---
