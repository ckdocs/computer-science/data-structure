# Generalized Linked List

**Generalized Linked List** is a type of **Linked List** with **Doubly** nodes that have a **Togglable Property** can have **Data or Link**:

-   If **tag** is **false** $\implies$ **down** is **Data**
-   If **tag** is **true** $\implies$ **down** is **Link**

![Generalized Linked List](../assets/generalized_linked_list.jpg)

```ts
class GeneralNode {
    tag: boolean;
    down: GeneralNode | string;
    next: GeneralNode;
}
```

---

## Concepts

There are many important concepts in **Generalized Linked List**

---

### Polynomials

We can represent the **Polynomials** using special version of **Generalized Linked List**:

![Polynomial](../assets/generalized_linked_list_polynomial.png)

Here flag means:

-   **0**: **Variable** is present
-   **1**: **Down Pointer** is present
-   **2**: **Coefficient and Exponent** is present

---

-   **Example**:

$$
\begin{aligned}
    & \texttt{Polynom}: 9x^5 + 7xy^4 + 10xz
\end{aligned}
$$

![Polynomial Representation](../assets/generalized_linked_list_polynomial_representation.png)

---
