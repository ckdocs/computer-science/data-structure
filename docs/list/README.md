# List

Is a sequence of **Linked Nodes** holds any number of **Values**

![List](../assets/list.png)

There are many concepts in **Lists**:

![Concepts](../assets/list_concepts.png)

1. **Node (Element)**: each node item has a **value** and one or more **links** to elements
2. **Head (Top, First)**: the **First Node** of the linked list called head, we store address of **Head** to access all items
3. **Tail (Bottom, Last)**: the **Last Node** of the linked list called tail, we can access it depends on the type of linked list

---

## Head (Top, First)

The `first` element(node) of the linked list called **head**, depends on the type of these nodes, linked list can be **singly** or **doubly**

1. **Singly**: If we save only address of **next**
2. **Doubly**: If we save address of **next** and **previous**

![Node](../assets/list_node.png)

---

## Tail (Bottom, Last)

The `last` element(node) of the linked list called **tail**, depends on the value of **next** in this element, linked list can be **linear** or **circular**

1. **Linear**: If next of tail goes to **NULL**
2. **Circular**: If next of tail goes to **Head**

---

## Top (New), Bottom (Old)

In **Array** and **List** structures we can define `top,bottom` concepts:

-   **Top**: Position of **new** items added
    -   In **Array**: increase index to **MAX** (**new** items added)
    -   In **List**: close to **Head** (**new** items added)
-   **Bottom**: Position of **old** items added
    -   In **Array**: decrease index to **0** (**old** items added)
    -   In **List**: close to **Tail** (**old** items added)

![Top Bottom](../assets/list_top_bottom.png)

And also there are 4 important methods:

1. **Push**: add to **top**
2. **Pop**: remove from **top**
3. **Unshift**: add to **bottom**
4. **Shift**: remove from **bottom**

Based on definition of **Top** and **Bottom** in **Arrays** and **Lists** they will act differently

---

## Optimization (Head,Tail)

In these operations we must get the **Tail** node:

1. **Push**
2. **Unshift**
3. **Pop**
4. **Shift**

For finding the **Tail** node we must iterate entire linked list, so this is the problem

If we save **Head** and **Tail** nodes we can optimize these operations to **O(1)**

In Circular linked lists we can, only save **Tail** node

---

## Available List (Avail)

It is a list of **free nodes** in the memory. It contains **unused memory cells** and these memory cells can be **used in future**. It is also known as **List of Available Space** or **Free Storage List** or **Free Pool**.

The operations using **Avail List** will implement in this way:

1. **Add**: add **HEAD** from **Avail List** to **Our List** item
    - If **Avail List** was empty and we want to **Add** item to our list, we get **Overflow** error
2. **Remove**: add **Item** from **Our List** to **Avail List** head
    - If **Our List** was empty and we want to **Remove** item from our list, we get **Underflow** error
    - ![Avail List Remove](../assets/list_avail_remove.jpeg)

![Avail List](../assets/list_avail.jpg)

---

## Unrolled List

A version of linked list that stores **Multiple Elements** in **Each Node**

It will **Optimize** the **Iteration** over linked list

![Unrolled List](../assets/list_unrolled.webp)

---

## Orders

![Orders](../assets/list_orders.png)

---
