# Doubly Linked List

**Doubly Linked List** is a type of **Linked List** with **Doubly** nodes and **Without Cycle**

![Doubly Linked List](../assets/doubly_linked_list.jpg)

---

## Add

-   **Add** like `singly linked list`
-   Set **Previous** link of `new object` to `previous object`
-   Set **Previous** link of `next object` to `new object`

```cpp
int size = 5;
struct Node* head = ...;

void add(int index, int value) {
    // Find previous node O(n)
    struct Node* prev = get(index - 1);

    // Create new node O(1)
    struct Node* node = malloc(sizeof(struct Node));
    node->prev = prev;
    node->next = prev->next;
    node->data = value;

    // Set previous node link O(1)
    prev->next = node;

    // Set next node link O(1)
    node->next->prev = node;

    // Increase size
    size++;
}
```

---

## Remove

-   **Remove** like `singly linked list`
-   Set **Next** link of `next object` to `previous object`

```cpp
int size = 5;
struct Node* head = ...;

void remove(int index) {
    // Find previous node O(n)
    struct Node* prev = get(index - 1);
    struct Node* old = prev->next;

    // Change previous link to next O(1)
    prev->next = prev->next->next;

    // Change next link to previous O(1)
    old->next->prev = prev;

    // Delete target node O(1)
    free(old);

    // Decrease size
    size--;
}
```

---
