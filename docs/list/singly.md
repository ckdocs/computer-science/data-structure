# Singly Linked List

**Singly Linked List** is a type of **Linked List** with **Singly** nodes and **Without Cycle**

![Singly Linked List](../assets/singly_linked_list.jpg)

---

## Concepts

There are many important concepts in **Singly Linked List**

---

### Polynomials

We can represent the **Polynomials** using special version of **Singly Linked List**

```ts
class PolynomNode {
    coef: Number;
    power: Number;
    next: PolynomNode;
}
```

![Polynomial](../assets/singly_linked_list_polynomial.png)

---

## Iterate

```cpp
int size = 5;
struct Node* head = ...;

void iterate() {
    struct Node* cursor = head;

    do {
        print(cursor->data);
    } while (cursor->next != NULL);
}
```

---

## Get

```cpp
int size = 5;
struct Node* head = ...;

int get(int index) {
    struct Node* cursor = head;

    // In linked-lists indexes are reverse
    while(size - index > 1) {
        cursor = cursor->next;
        index++;
    }

    return cursor;
}
```

---

## Set

```cpp
int size = 5;
struct Node* head = ...;

void set(int index, int value) {
    struct Node* item = get(index);

    item->data = value;
}
```

---

## Add

-   **Create** new node object
-   Set **Next** link of `new object` to `next of previous`
-   Set **Next** link of `previous object` to `new object`
-   **Increase** list size

![Add](../assets/singly_linked_list_add.png)

```cpp
int size = 5;
struct Node* head = ...;

void add(int index, int value) {
    // Find previous node O(n)
    struct Node* previous = get(index - 1);

    // Create new node O(1)
    struct Node* node = malloc(sizeof(struct Node));
    node->next = previous->next;
    node->data = value;

    // Set previous node link O(1)
    previous->next = node;

    // Increase size
    size++;
}
```

---

## Remove

-   **Find** target node object
-   Set **Next** link of `previous object` to `next`
-   **Delete** target object
-   **Decrease** list size

![Remove](../assets/singly_linked_list_remove.png)

```cpp
int size = 5;
struct Node* head = ...;

void remove(int index) {
    // Find previous node O(n)
    struct Node* previous = get(index - 1);
    struct Node* old = previous->next;

    // Change previous link to next O(1)
    previous->next = previous->next->next;

    // Delete target node O(1)
    free(old);

    // Decrease size
    size--;
}
```

---
