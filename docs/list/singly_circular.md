# Singly Circular Linked List

**Singly Circular Linked List** is a type of **Linked List** with **Singly** nodes and **With Cycle**

![Singly Circular Linked List](../assets/singly_circular_linked_list.jpg)

---

## Concepts

There are many important concepts in **Singly Circular Linked List**

---

### Loop detection

Imagine we have a linked list, we don't know anything about it's tail node (**Circluar** or **Linear**) and about it's size, how can we find it's **Circular** or **Linear**?

**Solution**: Create **Two** pointers to **Head**:

1. **Pointer 1**: Speed: 1x
2. **Pointer 2**: Speed: 2x

Start traversation of pointers, after a while if:

1. **Pointer 2** gots `NULL` $\implies$ it's **Linear**
2. **Pointer 2** gots **Pointer 1** $\implies$ it's **Circular**

---

### Josephus problem

Assume we have a circular linked list with nodes **1**, **2**, **3**, **4**, **5**, **6**

What is the result of this code?

```cpp
while(x->link != x) {
    x->link = x->link->link;
    x = x->link;
}

print(x->data);
```

This is **Josephus Problem**:

1. We have a **roundtable**
2. We start from **person 1**
3. Remove peoples **decussate**

Who is the final remaining person ?

![Josephus Problem](../assets/josephus-problem.png)

The solution of this problem is a recursive relation:

$$
\begin{aligned}
    & f(2n) = 2f(n) - 1
    \\
    & f(2n+1) = 2f(n) + 1
    \\
    & f(1) = 1
    \\
    & n: \texttt{Number of persons}
    \\
    \\
    & f(6) = 2f(3) - 1
    \\
    & f(3) = 2f(1) + 1
    \\ \implies
    & f(6) = 2(2*1 + 1) - 1 = 5
\end{aligned}
$$

> **Tip**
>
> In josephus problem, we must start from **person 1**, then remove **person 2**, if we start from another person, we must **Rotate** the roundtable and map person 1 to that person

---

**Tip**: Another fast way for solving **Josephus Problem** is **Binary Shifting**:

1. Write number of persons in **Binary format**
2. **Shift left**
3. Write number in **Decimal format**

**Example**: What is the remaining person in josephus problem with **75** persons:

$$
\begin{aligned}
    & 75 = 64 + 8 + 2 + 1
    \\
    & 75 = 1001011
    \\
    \\ \overset{\texttt{Left shift}}{\implies}
    & 0010111 = 1 + 2 + 4 + 16 = 23
    \\
    \\ \implies
    & f(75) = 23
\end{aligned}
$$

---
