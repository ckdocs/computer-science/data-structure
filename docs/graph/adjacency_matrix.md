# Adjacency Matrix

**Adjacency Matrix** is a type of **Graph** implemented with `2D static array`

-   Is a $|V|\times|V|$ size `matrix`
-   Value of $M[i][j]$ is:
    -   **0**: No edge between vertices **i** and **j**
    -   **x>1**: Edge of `weight x` between vertices **i** and **j**

![Adjacency Matrix](../assets/adjacency_matrix.png)

-   **Tip**: In `undirected graphs` the matrix is `symmetric`
-   **Tip**: Adjacency Matrices are better for **Dense Graphs** with **Space Complexity** of $O(n^2)$
-   **Tip**: In `adjacency matrix` implementation of algorithms we must replace $e$ order with $n^2$, because for finding **Edges**, we must iterate entire matrix
    -   $O(n + e) \implies O(n + n^2) = O(n^2)$

---

## Concepts

There are many important concepts in **Adjacency Matrix**

---

### Input, Output Degree

-   **Input Degree**: Number of `input neighbors`
-   **Output Degree**: Number of `output neighbors`

---

### UnDirected Graph

In undirected graphs we must add two **Edge** per each add:

-   **AddEdge(x, y)**
-   **AddEdge(y, x)**

---

### Path With Length

Assume **M** is the adjacency matrix of a **UnDirected Graph**:

-   $M^1$: Contains the **Number Of Pathes** with length of `1` between vertices
-   $M^2$: Contains the **Number Of Pathes** with length of `2` between vertices
-   ...
-   $M^k$: Contains the **Number Of Pathes** with length of `k` between vertices

---

## IsAdjacent

-   **Check** edge exists from vertex `from` to vertex `to` and return it's `weight`

```ts
let matrix = [
    //...
];

const isAdjacent = (from: number, to: number) => {
    return matrix[from][to];
};
```

---

## InputNeighbors

-   **Search** over `column`
-   **Add** edges with `non zero weight`

```ts
let matrix = [
    //...
];

const inputNeighbors = (vertex: number) => {
    let neighbors = [];

    for (let i = 0; i < matrix.length; i++) {
        if (matrix[i][vertex] > 0) {
            neighbors.push(i);
        }
    }

    return neighbors;
};
```

---

## OutputNeighbors

-   **Search** over `row`
-   **Add** edges with `non zero weight`

```ts
let matrix = [
    //...
];

const outputNeighbors = (vertex: number) => {
    let neighbors = [];

    for (let i = 0; i < matrix.length; i++) {
        if (matrix[vertex][i] > 0) {
            neighbors.push(i);
        }
    }

    return neighbors;
};
```

---

## AddVertex

```ts
let matrix = [
    //...
];

const addVertex = () => {
    // Resize the matrix
};
```

---

## AddEdge

```ts
let matrix = [
    //...
];

const addEdge = (from: number, to: number, weight: number) => {
    matrix[from][to] = weight;
};
```

---

## RemoveVertex

```ts
let matrix = [
    //...
];

const removeVertex = () => {
    // Resize the matrix
};
```

---

## RemoveEdge

```ts
let matrix = [
    //...
];

const removeEdge = (from: number, to: number) => {
    matrix[from][to] = 0;
};
```

---
