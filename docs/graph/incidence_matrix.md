# Incidence Matrix

**Incidence Matrix** is a type of **Graph** implemented with `2D static array`

-   Is a $|V|\times|E|$ size `matrix`
-   Value of $M[i][j]$ is:
    -   **0**: Edge `j` not connected to vertex `i`
    -   **1**: Edge `j` is output of vertex `i`
    -   **-1**: Edge `j` is input of vertex `i`

![Incidence Matrix](../assets/incidence_matrix.png)

---

## IsAdjacent

-   **Check** edge exists from vertex `from` to vertex `to` and return it's `weight`

```ts
let matrix = [
    //...
];

const isAdjacent = (from: number, to: number) => {
    for (let i = 0; i < matrix[0].length; i++) {
        if (matrix[from][i] === 1 && matrix[to][i] === -1) {
            return true;
        }
    }

    return false;
};
```

---

## InputNeighbors

-   **Search** over `edges` and find `inputs` of vertex

```ts
let matrix = [
    //...
];

const inputNeighbors = (vertex: number) => {
    let neighbors = [];

    for (let i = 0; i < matrix[0].length; i++) {
        if (matrix[vertex][i] === -1) {
            // Search over column, find other side of edge
            neighbors.push(other);
        }
    }

    return neighbors;
};
```

---

## OutputNeighbors

-   **Search** over `edges` and find `outputs` of vertex

```ts
let matrix = [
    //...
];

const inputNeighbors = (vertex: number) => {
    let neighbors = [];

    for (let i = 0; i < matrix[0].length; i++) {
        if (matrix[vertex][i] === 1) {
            // Search over column, find other side of edge
            neighbors.push(other);
        }
    }

    return neighbors;
};
```

---

## AddVertex

```ts
let matrix = [
    //...
];

const addVertex = () => {
    // Add row to matrix
};
```

---

## AddEdge

```ts
let matrix = [
    //...
];

const addEdge = (from: number, to: number, weight: number) => {
    matrix[from][weight] = 1;
    matrix[from][weight] = -1;
};
```

---

## RemoveVertex

```ts
let matrix = [
    //...
];

const removeVertex = () => {
    // Remove row, for each non empty edge, remove other side
};
```

---

## RemoveEdge

```ts
let matrix = [
    //...
];

const removeEdge = (from: number, to: number) => {
    // ???
};
```

---
