# Adjacency List

**Adjacency List** is a type of **Graph** implemented with `jagged array`

-   Is a $|V|$ size `array` contains `linked lists` or `dynamic arrays`
-   Items of $M[i]$ are `adjacents` of `i`

![Adjacency List](../assets/adjacency_list.png)

-   **Tip**: Adjacency Lists are better for **Sparse Graphs** with **Space Complexity** of $O(n+e)$
-   **Tip**: In `adjacency list` implementation of algorithms we should not replace $e$ order, because we access **Edges** in order $O(1)$
    -   $O(n + e) \implies O(n + e)$

---

## Concepts

There are many important concepts in **Adjacency List**

---

### Input, Output Degree

-   **Input Degree**: Number of `input neighbors`
-   **Output Degree**: Number of `output neighbors`

---

## IsAdjacent

-   **Check** edge exists from vertex `from` to vertex `to` and return it's `weight`

```ts
let list = [
    //...
];

const isAdjacent = (from: number, to: number) => {
    for (const vertex of list[from]) {
        if (vertex.key === to) {
            return vertex.weight;
        }
    }

    return 0;
};
```

---

## InputNeighbors

-   **Search** over `all sub-lists`
-   **Add** edges to `vertex`

```ts
let list = [
    //...
];

const inputNeighbors = (vertex: number) => {
    let neighbors = [];

    for (let i = 0; i < list.length; i++) {
        for (const vertex of list[i]) {
            if (vertex.key === vertex) {
                neighbors.push(i);
                break;
            }
        }
    }

    return neighbors;
};
```

---

## OutputNeighbors

-   **Return** vertex `sub-list`

```ts
let list = [
    //...
];

const outputNeighbors = (vertex: number) => {
    return list[vertex];
};
```

---

## AddVertex

```ts
let list = [
    //...
];

const addVertex = () => {
    // Resize the list
};
```

---

## AddEdge

```ts
let list = [
    //...
];

const addEdge = (from: number, to: number, weight: number) => {
    list[from].push({
        key: to,
        weight: weight,
    });
};
```

---

## RemoveVertex

```ts
let list = [
    //...
];

const removeVertex = () => {
    // Resize the list
};
```

---

## RemoveEdge

```ts
let list = [
    //...
];

const removeEdge = (from: number, to: number) => {
    list[from] = list[from].filter((vertex) => vertex.key != to);
};
```

---
