# Graph

Is a **non-linear** data structure consisting of **Nodes** that are connected to each other using **Edges**

$$
\begin{aligned}
    & G = \{V, E\}
    \\
    & |V| = n = \texttt{Number of vertices}
    \\
    & |E| = e = \texttt{Number of edges}
\end{aligned}
$$

There are many concepts in **Graphs**:

![Concepts](../assets/graph_concepts.png)

1. **Vertex**: A `node` of graph
2. **Edge**: A `line` between two vertices
3. **Degree**: Number of `connected edges` to a vertex in **UnDirected Graphs**
    - **Input Degree**: Number of `input edges` to a vertex in **Directed Graphs**
    - **Output Dergree**: Number of `output edges` to a vertex in **Directed Graphs**
4. **Path**: Sequence of `edges` between the `two vertices`
5. **Cycle**: A `path` from and to the `same vertex`
6. **Adjacent**: Vertex `a` is adjacent to `b` if there is an edge from `a` to `b`
7. **Neighbors**: All the `adjacent nodes` of a vertex

---

## Connected, Disconnected

-   **Connected Graph**: A graph which we can find at least one **Path** between `each` **Two Vertices**
-   **Disconnected Graph**: A graph with multiple **Partitions** (**Connected Graphs**)

![Connected, Disconnected](../assets/graph_connected_disconnected.png)

---

## Weighted, UnWeighted

-   **Weighted Graph**: A graph that each **Edge** has a value (`weight`)
-   **UnWeighted Graph**: A graph that `weights` of **All Edges** is **1**

![Weighted, UnWeighted](../assets/graph_weighted_unweighted.png)

-   **Tip**: The weight `0` means no edge

---

## Directed, UnDirected

-   **Directed Graph**: A graph with **Directed** `edges`
-   **UnDirected Graph**: A graph that `edges` are **Straight** and **Reverse**

![Directed, UnDirected](../assets/graph_directed_undirected.png)

-   **Tip**: In directed graphs, edges are not equal to their reverse:

$$
\begin{aligned}
    & \texttt{Directed Graph}: (a,b) \neq (b,a)
    \\
    \\
    & \texttt{UnDirected Graph}: (a,b) = (b,a)
\end{aligned}
$$

---

## Cyclic, Acyclic

-   **Cyclic Graph**: A graph containing at least one **Graph Cycle** `path`
-   **Acyclic Graph**: A graph without **Graph Cycle** `path`

![Cyclic, Acyclic](../assets/graph_cyclic_acyclic.png)

---

## Dense, Sparse

-   **Dense Graph**: A graph that `number of edges` is close to **Maximum**
    -   $e \in O(n)$
-   **Sparse Graph**: A graph that `number of edges` is close to **Minimum**
    -   $e \in O(n^2)$

![Dense, Sparse](../assets/graph_dense_sparse.png)

---

## Orders

![Orders](../assets/graph_orders.png)

---
