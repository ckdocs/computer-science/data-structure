# Queue

Is a **ADT (Abstract Data Type)** works in **FIFO (First In First Out)** or **LILO (Last In Last Out)**

![Queue](../assets/queue.jpg)

There are many concepts in **Queues**:

![Concepts](../assets/queue_concepts.jpg)

1. **Front**: `begin` of list where we `pop from` it
2. **Rear**: `end` of list where we `unshift to` it

---

## Front (Top)

The front or begin of **Queue** where we pop elements from it:

1. **Start** of array list (**Start** - **Index 0**)
2. **Head** of linked list (**Start**)
3. By **removing** elements front will **increase**

---

## Rear (Bottom)

The rear or end of **Queue** where we unshift elements to it:

1. **End** of array list (**End** - **Index MAX**)
2. **Tail** of linked list (**End**)
3. By **adding** elements rear will **increase**

---

## Enqueue, Dequeue

The main operations of queue are:

1. **Enqueue**: or **unshift to bottom** method
    - ![Enqueue](../assets/queue_enqueue_diagram.jpg)
2. **Dequeue**: or **pop from top** method
    - ![Dequeue](../assets/queue_dequeue_diagram.jpg)

---

## Orders

![Orders](../assets/queue_orders.png)

---
