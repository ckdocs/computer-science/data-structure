# Two-Stack Queue

**Two-Stack Queue** is a type of **Queue** implemented with `two stacks`

![Two Stack Queue](../assets/two_stack_queue.png)

---

## Enqueue

-   **Push** to `stack 1`

```cpp
struct Stack* stack1 = ...;
struct Stack* stack2 = ...;

void enqueue(int value) {
    if (stack1.isFull()) {
        throw "Queue overflow";
    }

    stack1.push(value);
}
```

---

## Dequeue

-   If `stack 2` is empty
    -   **Pop** to empty from `stack 1` to `stack 2`
-   **Pop** from `stack 2`

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int dequeue() {
    if (stack2.isEmpty()) {
        while (!stack1.isEmpty()) {
            stack2.push(stack1.pop());
        }
    }

    if (stack2.isEmpty()) {
        throw "Queue underflow";
    }

    return stack2.pop();
}
```

---
