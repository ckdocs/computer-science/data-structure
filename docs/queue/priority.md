# Priority Queue

**Priority Queue** is a type of **Queue** that items will `enqueue or dequeue` by their `priority order`

---

## Concepts

There are many important concepts in **Priority Queue**

---

### Implementation

There are many ways to implement a `priority queue`:

-   **Insertion Sort**: Ordering when `enqueue`
-   **Selection Sort**: Ordering when `dequeue`
-   **Binary Search Tree**: Ordering using `BST` or `AVL` or `Red-Black`
-   **Binary Heap**: Ordering using `Binary Heap`

---
