# Linked Queue

**Linked Queue** is a type of **Queue** implemented with `optimized singly linked list`

![Linked Queue](../assets/linked_queue.png)

---

## Concepts

There are many important concepts in **Linked Queue**

---

### Front, Rear

-   The `front` node is **Head** in `linked list`
-   The `rear` node is **Tail** in `linked list`

---

## IsEmpty

```cpp
struct Node* head = ...;
struct Node* tail = ...;

bool isEmpty() {
    return head->next == NULL;
}
```

---

## IsFull

```cpp
struct Node* head = ...;
struct Node* tail = ...;

bool isFull() {
    return false;
}
```

---

## Enqueue

```cpp
struct Node* head = ...;
struct Node* tail = ...;

void enqueue(int value) {
    if (isFull()) {
        throw "Queue overflow";
    }

    struct Node* node = malloc(sizeof(struct Node));
    node->next = NULL;
    node->data = value;

    tail->next = node;
    tail = node;
}
```

---

## Dequeue

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int dequeue() {
    if (isEmpty()) {
        throw "Queue underflow";
    }

    struct Node* node = head->next;
    head->next = node->next;

    int result = node->data;
    free(node);
    return result;
}
```

---

## Peek

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int peek() {
    if (isEmpty()) {
        throw "Queue underflow";
    }

    return head->next->data;
}
```

---
