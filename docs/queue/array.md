# Array Queue

**Array Queue** is a type of **Queue** implemented with `static array`

![Array Queue](../assets/array_queue.png)

---

## Concepts

There are many important concepts in **Array Queue**

---

### Front, Rear

We must save two variables named **Front** and **Rear**:

1. **array list**
2. **front** variable
3. **rear** variable
4. **cyclic** increment of front and rear

![Front Rear](../assets/array_queue_front_rear.png)

---

## IsEmpty

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

bool isEmpty() {
    return rear == front;
}
```

---

## IsFull

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

bool isFull() {
    // Array queues, always have one empty block to determine the full state (rear + 1 = front)
    return (rear + 1) % 100 == front;
}
```

---

## Enqueue

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

void enqueue(int value) {
    if (isFull()) {
        throw "Queue overflow";
    }

    rear = (rear + 1) % n;
    array[rear] = value;
}
```

---

## Dequeue

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

int dequeue() {
    if (isEmpty()) {
        throw "Queue underflow";
    }

    int result = array[front];
    front = (front + 1) % n;
    return result;
}
```

---

## Peek

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

int peek() {
    if (isEmpty()) {
        throw "Queue underflow";
    }

    return array[front];
}
```

---
