# Linked Deque

**Linked Deque** is a type of **Deque** implemented with `optimized doubly linked list`

![Linked Deque](../assets/linked_deque.png)

---

## Concepts

There are many important concepts in **Linked Deque**

---

### Front, Rear

-   The `front` node is **Head** in `linked list`
-   The `rear` node is **Tail** in `linked list`

---

## IsEmpty

```cpp
struct Node* head = ...;
struct Node* tail = ...;

bool isEmpty() {
    return head->next == NULL;
}
```

---

## IsFull

```cpp
struct Node* head = ...;
struct Node* tail = ...;

bool isFull() {
    return false;
}
```

---

## Push

```cpp
struct Node* head = ...;
struct Node* tail = ...;

void push(int value) {
    if (isFull()) {
        throw "Deque overflow";
    }

    struct Node* node = malloc(sizeof(struct Node));
    node->next = head->next;
    node->prev = NULL;
    node->data = value;

    head->next = node;
}
```

---

## Pop

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int pop() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    struct Node* node = head->next;
    head->next = node->next;
    head->next->prev = NULL;

    int result = node->data;
    free(node);
    return result;
}
```

---

## Unshift

```cpp
struct Node* head = ...;
struct Node* tail = ...;

void unshift(int value) {
    if (isFull()) {
        throw "Deque overflow";
    }

    struct Node* node = malloc(sizeof(struct Node));
    node->next = NULL;
    node->prev = tail;
    tail->next = node;

    tail = node;
}
```

---

## Shift

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int shift() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    struct Node* node = tail;
    tail->prev->next = NULL;
    tail = tail->prev;

    int result = node->data;
    free(node);
    return result;
}
```

---

## Top

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int top() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    return head->next->data;
}
```

---

## Bottom

```cpp
struct Node* head = ...;
struct Node* tail = ...;

int bottom() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    return tail->data;
}
```

---
