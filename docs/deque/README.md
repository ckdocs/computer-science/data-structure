# Deque

**Deque** or **Dequeue** or **Double Ended Queue** is a bi-directional queue, we can **add** and **remove** items from/to **front** and **rear**

![Deque](../assets/deque.png)

There are many concepts in **Deque**:

1. **Front**: `begin` of list where we `pop from` it or `push to` it
2. **Rear**: `end` of list where we `shift from` it or `unshift to` it

---

## Orders

![Orders](../assets/deque_orders.png)

---
