# Array Deque

**Array Deque** is a type of **Deque** implemented with `static array`

![Array Deque](../assets/array_deque.png)

---

## Concepts

There are many important concepts in **Array Deque**

---

### Front, Rear

We must save two variables named **Front** and **Rear**:

1. **array list**
2. **front** variable
3. **rear** variable
4. **cyclic** increment of front and rear

---

## IsEmpty

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

bool isEmpty() {
    return rear == front;
}
```

---

## IsFull

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

bool isFull() {
    return (rear + 1) % 100 == front;
}
```

---

## Push

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

void push(int value) {
    if (isFull()) {
        throw "Deque overflow";
    }

    front = (front - 1) % n;
    array[front] = value;
}
```

---

## Pop

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

int pop() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    int result = array[front];
    front = (front + 1) % n;
    return result;
}
```

---

## Unshift

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

void unshift(int value) {
    if (isFull()) {
        throw "Deque overflow";
    }

    rear = (rear + 1) % n;
    array[rear] = value;
}
```

---

## Shift

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

int shift() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    int result = array[rear];
    rear = (rear - 1) % n;
    return result;
}
```

---

## Top

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

int top() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    return array[front];
}
```

---

## Bottom

```cpp
int front = 3;
int rear = 6;
int array[100] = {0,0,0,10,21,33,9,0,0,...};

int bottom() {
    if (isEmpty()) {
        throw "Deque underflow";
    }

    return array[rear];
}
```

---
