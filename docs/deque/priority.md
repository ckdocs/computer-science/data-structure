# Priority Deque

**Priority Deque** is a type of **Deque** that items will `add or remove` by their `priority order`

---

## Concepts

There are many important concepts in **Priority Deque**

---

### Implementation

There are many ways to implement a `priority deque`:

-   **Insertion Sort**: Ordering when `push` or `unshift`
-   **Selection Sort**: Ordering when `pop` or `shift`
-   **Binary Search Tree**: Ordering using `BST` or `AVL` or `Red-Black`
-   **Deap**: Ordering using `Deap`
-   **Min-Max Heap**: Ordering using `Min-Max Heap`

---
