# Hash Set

**Hash Set** is a type of **Set** with **Fast Access** and **Lower Space**

![Hash Set](../assets/hash_set.png)

-   **Tip**: We will store only **Keys** in each node
-   **Tip**: The new elements will add at the **First Of Linked List** with **O(1)**

---

## Search

-   **Find** linked list by `hash`
-   **Iterate** list to find `key` node

```cpp
struct LinkedList* set[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

int search(int key) {
    int index = hash(key);
    struct LinkedList* list = set[index];
    struct LinkedNode* node = list->head;

    // Iterate linked list to find key node
    while(node != NULL && node->key != key) {
        node = node->next;
    }

    // Not found
    if (node == NULL) {
        return -1;
    }

    // Found
    return 1;
}
```

---

## Add

-   **Find** linked list by `hash`
-   **Iterate** list to find `key` index
-   If **Not Found** `add` it to list

```cpp
struct LinkedList* set[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

int add(int key) {
    int index = hash(key);
    struct LinkedList* list = set[index];

    // Iterate linked list to find key node
    while(node->next != NULL && node->next->key != key) {
        node = node->next;
    }

    // Not found
    if (node->next == NULL) {
        list->add(linkednode_new(key));
    }
}
```

---

## Remove

-   **Find** linked list by `hash`
-   **Iterate** list to find `key` index
-   **Remove** item at index

```cpp
struct LinkedList* set[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

int remove(int key) {
    int index = hash(key);
    struct LinkedList* list = set[index];

    // Iterate linked list to find key node
    while(node->next != NULL && node->next->key != key) {
        node = node->next;
    }

    // Not found
    if (node->next == NULL) {
        return -1;
    }

    struct LinkedNode* old = node->next;
    node->next = node->next->next;
    free(old);
}
```

---

## Union

-   **Iterate** `second set`
-   **Add** items to `first set`

```cpp
struct LinkedList* set[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

void union_(Set* set) {
    ...
}
```

---

## Intersection

-   **Iterate** `first set`
-   If key **Not Found** in `second set`
    -   **Remove** key

```cpp
struct LinkedList* set[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

void intersection(Set* set) {
    ...
}
```

---

## Difference

-   **Iterate** `first set`
-   If key **Found** in `second set`
    -   **Remove** key

```cpp
struct LinkedList* set[10] = {
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new(),
    linkedlist_new()
};

void difference(Set* set) {
    ...
}
```

---
