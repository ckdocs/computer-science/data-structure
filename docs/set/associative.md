# Associative Set

**Associative Set** or **Associative Array** or **Multi Set** is a type of **Set** which keys are stored in **Items** of a `array` or `linked list`

![Associative Set](../assets/associative_set.png)

---

## Iterate

-   **Iterate** through the `set size` and `print key`

```cpp
struct LinkedList* set = ...;

void iterate() {
    struct LinkedNode* node = set->head;

    while (node != NULL) {
        print(node->key);
        node = node->next;
    }
}
```

---

## Search

-   **Iterate** through the `set size` and `find key`

```cpp
struct LinkedList* set = ...;

int search(int key) {
    struct LinkedNode* node = set->head;

    while (node != NULL) {
        if (node->key == key) {
            return 1;
        }

        node = node->next;
    }

    return 0;
}
```

---

## Add

-   **Iterate** through the `set size`
-   If key **Not Found**, `add` it

```cpp
struct LinkedList* set = ...;

void add(int key) {
    struct LinkedNode* node = set->head;

    do {
        if (node->key == key) {
            return;
        }
    } while (node->next != NULL);

    node->next = linkednode_new(key);
}
```

---

## Remove

-   **Iterate** through the `set size`
-   If key **Found**, `remove` it

```cpp
struct LinkedList* set = ...;

void remove(int key) {
    struct LinkedNode* node = set->head;

    while (node->next != NULL) {
        if (node->next->key == key) {
            struct LinkedNode* target = node->next;
            node->next = node->next->next;
            free(target);

            return;
        }
    }
}
```

---

## Union

-   **Iterate** `second set`
-   **Add** items to `first set`

```cpp
struct LinkedList* set = ...;

void union_(Set* set) {
    ...
}
```

---

## Intersection

-   **Iterate** `first set`
-   If key **Not Found** in `second set`
    -   **Remove** key

```cpp
struct LinkedList* set = ...;

void intersection(Set* set) {
    ...
}
```

---

## Difference

-   **Iterate** `first set`
-   If key **Found** in `second set`
    -   **Remove** key

```cpp
struct LinkedList* set = ...;

void difference(Set* set) {
    ...
}
```

---
