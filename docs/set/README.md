# Set

**Set** is a structure for saving **Unique Keys** and `searching` by **Unique Keys** without `duplicate` and `order`

![Set](../assets/set.png)

---

## Operations

There are three main operations cross sets:

-   **Union**
-   **Intersection**
-   **Difference**

---

## Orders

![Orders](../assets/set_orders.png)

![Disjoint Sets Orders](../assets/disjoint_sets_orders.png)

---
