# Tree Set

**Tree Set** or **Search Tree Set** is a type of **Set** which use a **Binary Search Tree** in it's core for finding `keys`

![Tree Set](../assets/tree_set.png)

---

## Concepts

There are many important concepts in **Tree Set**

---

### Key Only

-   In **Tree Set** structures, we store a `key only` in each node

---

## Iterate

-   **Traverse** tree `DFS` or `BFS`

```cpp
struct BST* bst = ...;

void iterate() {
    return bst.inOrderDFS();
}
```

---

## Search

```cpp
struct BST* bst = ...;

int search(int key) {
    return bst.search(key);
}
```

---

## Add

```cpp
struct BST* bst = ...;

void add(int key) {
    bst.add(key);
}
```

-   **Tip**: In adding, if a same key **Exists** it will not add (`No duplication`)

---

## Remove

```cpp
struct BST* bst = ...;

void remove(int key) {
    bst.remove(key);
}
```

---

## Union

-   **Iterate** `second set`
-   **Add** items to `first set`

```cpp
struct BST* bst = ...;

void union_(Set* set) {
    ...
}
```

---

## Intersection

-   **Iterate** `first set`
-   If key **Not Found** in `second set`
    -   **Remove** key

```cpp
struct BST* bst = ...;

void intersection(Set* set) {
    ...
}
```

---

## Difference

-   **Iterate** `first set`
-   If key **Found** in `second set`
    -   **Remove** key

```cpp
struct BST* bst = ...;

void difference(Set* set) {
    ...
}
```

---
