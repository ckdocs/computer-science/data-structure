# Disjoint Sets

Is an structure like **Map** for saving `multiple sets` in one structure and operating **Union** faster

-   Store **Parent** of each `key` as it's `value`
-   If `key` was **Agent** of set, set `parent` as `self key`

![Disjoint Sets](../assets/disjoint_sets.png)

We can convert any `set` structure to `disjoint sets` by storing the **Parent** (`set`) of each **Key**

---

## Concepts

There are many important concepts in **Disjoint Sets**

---

### Agent

Each set as an **Agent** that is `final parent` of all keys

![Agent](../assets/disjoint_sets_agent.png)

---

## MakeSet

Create a `new set` with `one key`:

-   **Set** key's `parent` to itself
-   Init **Size** or **Rank** if exists

```ts
const makeSet = (sets: Sets, key: number) => {
    sets[key] = key;
    sets[key].size = 1;
    sets[key].rank = 0;
};
```

---

## FindSet

Return `agent` of `key`:

-   **While** key's `parent` is not self
-   **Goto** key's parent

```ts
const findSet = (sets: Sets, key: number) => {
    const parent = sets[key];

    // Recursive call parent
    if (parent != key) {
        return findSet(sets, parent);
    }

    return sets[key];
};
```

---

### Path Compression

-   **While** moving to `parent`
-   **Update** nodes parents, set to `agent`

![Path Compression](../assets/disjoint_sets_path_compression.png)

```ts
const findSet = (sets: Sets, key: number) => {
    const parent = sets[key];

    // Update parent to agent
    if (parent != key) {
        sets[key] = findSet(sets, parent);
    }

    return sets[key];
};
```

-   **Tip**: If a `Disjoint Set` uses the **Path Compression** and **Union By X** method, the complexity of **FindSet** method is:

$$
\begin{aligned}
    & \texttt{Best Case}: O(1)
    \\
    & \texttt{Worst Case}: O(\log{n})
    \\
    & \texttt{Amortized}: O(\alpha) \simeq O(\log^{*}{n}) \simeq O(1)
\end{aligned}
$$

---

## Union

-   **FindSet** for `key1`
-   **FindSet** for `key2`
-   **Set** `agent` of one set to another

![Union](../assets/disjoint_sets_union.png)

```ts
const union = (sets: Sets, key1: number, key2: number) => {
    key1 = findSet(sets, key1);
    key2 = findSet(sets, key2);

    // Set agent of one set to another
    sets[key1] = key2;

    // Update size or rank of new agent
    sets[key2].size = ...;
    sets[key2].rank = ...;
};
```

-   **Tip**: If the keys are **Agent** of sets, the complexity of union is $O(1)$
-   **Tip**: There are two method to decide which parent is `better` to select, to **Optimize** to union (`Reduce height`)

---

### Union By Size

**Size** is number of **keys** in the set

-   **Select** the parent with **Lower Size** as agent
-   **Update** the size of new agent and add other size

![Union By Size](../assets/disjoint_sets_union_by_size.png)

```ts
const union = (sets: Sets, key1: number, key2: number) => {
    key1 = findSet(sets, key1);
    key2 = findSet(sets, key2);

    // Select biggest size as new agent
    if (sets[key1].size < sets[key2].size) {
        sets[key1] = key2;

        // Update size and add other agent size
        sets[key2].size += sets[key1].size;
    } else {
        sets[key2] = key1;

        // Update size and add other agent size
        sets[key1].size += sets[key2].size;
    }
};
```

-   **Tip**: In the `worst case` we union items **Doubly**, and the `tree height` is increased by $\log{n}$

---

### Union By Rank

Or called **Weighted Union**

**Rank** is tree **height** of set

-   **Select** the parent with **Lower Rank** as agent
-   **Check** the ranks of two sets is **Equal**:
    -   **Add** `1` to rank of new agent

![Union By Rank](../assets/disjoint_sets_union_by_rank.png)

```ts
const union = (sets: Sets, key1: number, key2: number) => {
    key1 = findSet(sets, key1);
    key2 = findSet(sets, key2);

    // Select biggest rank as new agent
    if (sets[key1].rank < sets[key2].rank) {
        sets[key1] = key2;
    } else if (sets[key1].rank > sets[key2].rank) {
        sets[key2] = key1;
    } else {
        sets[key2] = key1;

        // Update rank if ranks are equal
        sets[key1].rank++;
    }
};
```

-   **Tip**: In the `worst case` we union items **Doubly**, and the `tree height` is increased by $\log{n}$

---
