# Direct Set

**Direct Set** or **Enum Set** or **VectorBit Set** is a type of **Set** which keys are **Array Index** and values are **Boolean** (`Exists` in set or not)

![Direct Set](../assets/direct_set.png)

We use **Direct Set** when:

1. the keys are **small integers**
2. the number of keys is **not too large**
3. no two data have the **same key**
4. keys are **enumerates** (limited strings)

---

## Concepts

There are many important concepts in **Direct Set**

---

### Searching

-   For **Searching** operation we must search in `get by key` with complexity of $O(1)$
-   **Memory Loss**: Key's domain may be large, so we need to allocate a **Large Set** with a lot of empty elements
    -   For fixing this problem we use **Hash Set** to map the key's domain into smaller domain

---

## Iterate

-   **Iterate** through the `set size`
    -   If **Is 1** print key

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

void iterate() {
    for (int cursor = 0 ; cursor < 10 ; cursor++) {
        if (set[cursor] == 1) {
            print(cursor);
        }
    }
}
```

---

## Search

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

int search(int key) {
    return set[key];
}
```

---

## Add

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

void add(int key) {
    set[key] = 1;
}
```

---

## Remove

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

void remove(int key) {
    set[key] = 0;
}
```

---

## Union

-   **Iterate** `second set`
-   **Add** items to `first set`

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

void union_(Set* set) {
    struct Iterator* iterator = set->iterator();

    while (iterator != NULL) {
        add(set, iterator->key);
        iterator = iterator->next();
    }
}
```

---

## Intersection

-   **Iterate** `first set`
-   If key **Not Found** in `second set`
    -   **Remove** key

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

void intersection(Set* set) {
    ...
}
```

---

## Difference

-   **Iterate** `first set`
-   If key **Found** in `second set`
    -   **Remove** key

```cpp
int set[10] = {0,1,0,0,1,0,0,1,0,0};

void difference(Set* set) {
    ...
}
```

---
