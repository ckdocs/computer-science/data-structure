# Array

Is a container or **List** holds a **Fixed Number** of items with **Same Type** and **Same Size** in **Order**

![Array](../assets/array.png)

There are many concepts in **Arrays**:

![Concepts](../assets/array_concepts.jpg)

1. **Start Index**: the index of first element, may be 0 or 1 (language dependent) (**Not Important!**)
2. **Memory**: when saving multi-dimensional arrays into memory, it will splited by rows or columns
3. **Address**
    - **Base Address**: address of the **First Element** of array in **Memory**
    - **Item Address**: address of the **Specific Element** of array in **Memory**

---

## Representation

We can show arrays in two different formats:

1. **Size**: Size of the dimension
2. **Range**: Lower and Upper of the dimension (**Index Range**)

$$
\begin{aligned}
    & \texttt{Array}[\texttt{Size}_1, \texttt{Size}_2, \texttt{Size}_3, \dots]
    \\
    \\
    & \texttt{Array}[\texttt{Range}_1, \texttt{Range}_2, \texttt{Range}_3, \dots]
    \\
    \\
    & A = \texttt{Array}[5, 3, 2];
    \\
    \\
    & B = \texttt{Array}[2..6, 3..5, 0..1];
\end{aligned}
$$

-   **Tip**: For **Simplifing** the problems we can convert **Range** format to **Size** format before the begin:

$$
\begin{aligned}
    & \texttt{Array}[2..6, 3..5, 0..1]
    \\ =
    & \texttt{Array}[6-2+1, 5-3+1, 1-0+1]
    \\ =
    & \texttt{Array}[5, 3, 2]
\end{aligned}
$$

---

## Memory

We can save **N Dimensional** arrays in a **Linear Memory** by spliting it into **Rows** or **Columns**

Saving arrays in a **Linear Memory** is a bit tricky, we must convert **Multi Dimensional Array** to a **Linear Array**

![Memory](../assets/array_memory.png)

This convertion can be in two different formats depends on the **Compiler** we use:

1. **Row Major**
2. **Column Major**

---

### Row Major (Binary Increase)

The matrix will save in **sequence of rows**:

![Row Major](../assets/array_row_major.png)

In multi dimensional arrays this major can represented in this format:

-   Increase from **Upper** dimension to **Lower** dimension

$$
\begin{aligned}
    & A = \texttt{Array}[3,3];
    \\
    & A = A_{0,0}, A_{0,1}, A_{0,2}, A_{1,0}, A_{1,1}, A_{1,2}, A_{2,0}, A_{2,1}, A_{2,2}
    \\
    & A_{i,j} = (i*3) + (j)
    \\
    \\
    & A = \texttt{Array}[4,5,6];
    \\
    & A = A_{0,0,0}, A_{0,0,1}, A_{0,0,2}, \dots, A_{0,1,0}, A_{0,1,1}, \dots, A_{3,4,5}
    \\
    & A_{i,j,k} = (i*5*6) + (j*6) + (k)
\end{aligned}
$$

---

### Column Major (Reverse Increase)

The matrix will save in **sequence of columns**:

![Column Major](../assets/array_column_major.png)

In multi dimensional arrays this major can represented in this format:

-   Increase from **Lower** dimension to **Upper** dimension

$$
\begin{aligned}
    & A = \texttt{Array}[3,3];
    \\
    & A = A_{0,0}, A_{1,0}, A_{2,0}, A_{0,1}, A_{1,1}, A_{2,1}, A_{0,2}, A_{1,2}, A_{2,2}
    \\
    & A_{i,j} = i + (j*3)
    \\
    \\
    & A = \texttt{Array}[4,5,6];
    \\
    & A = A_{0,0,0}, A_{1,0,0}, A_{2,0,0}, \dots, A_{0,1,0}, A_{1,1,0}, \dots, A_{3,4,5}
    \\
    & A_{i,j,k} = (i) + (j*4) + (k*5*4)
\end{aligned}
$$

---

## Address

Every data structure will store in a **Linear Memory**, so every **Data Element** have an **Position** in array, and an **Address** in memory

![Address](../assets/array_address.png)

What is the **Position** and **Address** of `A[i]` in `A[L..U]`?

$$
\begin{aligned}
    & \texttt{Index A[i]} = (i - L)
    \\
    \\
    & \texttt{Position A[i]} = Index + 1
    \\
    & \texttt{Address A[i]} = Index * ElementSize + BaseAddress
    \\
    \\
    \\
    & A = \texttt{Array}[4,5,3];
    \\
    & Index(A_{i,j,k}) = (i) + (j*4) + (k*5*4)
    \\
    \\
    & Position(A_{i,j,k}) = Index(A_{i,j,k}) + 1
    \\
    & Address(A_{i,j,k}) = Index(A_{i,j,k}) * 2 + 1000
\end{aligned}
$$

---

### Example 1

We have an array **A[1..10, -2..2, 1..15, 0..9]**, we have an element **A[3,1,5,4]**:

1. What is the **Position** of this element in **Row Major**
2. If **ElementSize = 2byte**, **BaseAddress = 1000**, what is the **Address** of this element in **Column Major**?

$$
\begin{aligned}
    & A = \texttt{Array}[1..10, -2..2, 1..15, 0..9] = \texttt{Array}[10, 5, 15, 10]
    \\
    & A_{3,1,5,4} = A_{3-1, 1-(-2), 5-1, 4-0} = A_{2,3,4,4}
    \\
    \\
    & A_{2,3,4,4}^{Row Major} = (2*5*15*10) + (3*15*10) + (4*10) + (4) = 1994
    \\
    & A_{2,3,4,4}^{Column Major} = (2) + (3*10) + (4*5*10) + (4*15*5*10) = 3232
    \\
    \\
    & Position(A_{Row Major}) = 1994 + 1 = 1995
    \\
    & Address(A_{Column Major}) = 3232 * 2 + 1000 = 7464
\end{aligned}
$$

---

### Example 2

We have a **Lower Triangular Matrix**, what is the **Index** of **A[i,j]** if we save it in:

![Example2](../assets/array_example2.png)

1. **Row Major**:

$$
\begin{aligned}
    & A = A_{0,0}, A_{1,0}, A_{1,1}, A_{2,0}, A_{2,1}, A_{2,2}, \dots, A_{n,0}, A_{n,1}, \dots, A_{n,n}
    \\
    & Index(A_{i,j}) = \sum^{i} (1 + 2 + 3 + \dots) + j
\end{aligned}
$$

2. **Column Major**

$$
\begin{aligned}
    & A = A_{0,0}, A_{0,1}, A_{0,2}, \dots, A_{0,n}, A_{1,1}, A_{1,2}, \dots, A_{n,n}
    \\
    & Index(A_{i,j}) = \sum^{j} (n + (n-1) + (n-2) + \dots) + i
\end{aligned}
$$

3. **Diagonal Major**

$$
\begin{aligned}
    & A = A_{0,0}, A_{1,1}, A_{2,2}, A_{3,3}, \dots, A_{n,n}, A_{1,0}, A_{2,1}, \dots, A_{n,n-1}, A_{2,0}, \dots, A_{n,n}
    \\
    & Index(A_{i,j}) = \sum^{i-j} (n + (n-1) + (n-2) + \dots) + j
\end{aligned}
$$

---

## Orders

![Orders](../assets/array_orders.png)

---
