# Static Array

**Static Array** or **Array** is a type of **Array** with `fixed` size or length

![Static Array](../assets/static_array.svg)

---

## Concepts

There are many important concepts in **Static Array**

---

### Polynomials

We can represent the **Polynomials** using **Static Arrays** by saving **coefficients** in **indices of exponents** in an **Array**

$$
\begin{aligned}
    & \texttt{Polynom}: 9 + 7x + 6x^2 + 4x^3
\end{aligned}
$$

![Polynomial](../assets/static_array_polynomial.png)

-   **Tip**: This type of **Polynomial Representation** can be inefficient in **Space Complexity**
    -   For saving $6 + x^{1000}$ we must allocate an array with size of **1000**

---

## Iterate

![Iterate](../assets/static_array_iterate.png)

```cpp
int top = 5;
int array[10] = {1,2,3,4,5,0,0,0,0,0};

void iterate() {
    for (int cursor = 0 ; cursor < top ; cursor++) {
        print(array[cursor]);
    }
}
```

-   **Tip**: We can implement higher level operations using array iterations in $O(n)$:
    -   **Contains**: Check value exists in array
    -   **IndexOf**: Get index of a value

---

## Get

```cpp
int top = 5;
int array[10] = {1,2,3,4,5,0,0,0,0,0};

int get(int index) {
    return array[index];
}
```

---

## Set

```cpp
int top = 5;
int array[10] = {1,2,3,4,5,0,0,0,0,0};

void set(int index, int value) {
    array[index] = value;
}
```

---

## Add

-   **Right Shift** the `index`
-   **Set** the `index` value to **Value**
-   **Increase** array top

![Add](../assets/static_array_add.jpg)

```cpp
int top = 5;
int array[10] = {1,2,3,4,5,0,0,0,0,0};

void add(int index, int value) {
    rightShift(array[index]);

    array[index] = value;

    top++;
}
```

---

## Remove

-   **Decrease** array top
-   **Set** the `index` value to **0**
-   **Left Shift** the `index`

![Remove](../assets/static_array_remove.png)

```cpp
int top = 5;
int array[10] = {1,2,3,4,5,0,0,0,0,0};

void remove(int index) {
    top--;

    array[index] = 0;

    leftShift(array[index]);
}
```

---
