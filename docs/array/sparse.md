# Sparse Array

**Sparse Array** or **Sparse Matrix** is an array of data in which many elements have a value of zero

We save **Identifier, Element** in a linear array instead of a big multidimensional array:

![Sparse Array](../assets/sparse_array.png)

---

## Concepts

There are many important concepts in **Sparse Array**

---

### Polynomials

We can represent the **Polynomials** using **Sparse Arrays** by saving **coefficients** and **exponents** tuples in a **Matrix**

$$
\begin{aligned}
    & \texttt{Polynom}: 2x^{1000} + 1 + x^4 + 10x^3 + 3x^2
\end{aligned}
$$

![Polynomial](../assets/sparse_array_polynomial.png)

-   **Tip**: This type of **Polynomial Representation** can be inefficient in **Time Complexity**
    -   For finding the **Coefficient of exponent x** we must search entire array with $O(n)$

---

### Sparse matrix

Storing Sparse matrices in **Sparse Arrays** is the same, with an **additional row**

The rules of sparse matrix are:

1. Additional **First row**:
    1. **Number of rows**
    2. **Number of columns**
    3. **Number of non-zero elements**
2. Store non-zero items **Row major**

Of the matrix in first row of **Sparse Array**

![Sparse Matrix](../assets/sparse_matrix.png)

$$
\begin{aligned}
    & \texttt{First row}: \{5, 6, 6\}
    \\
    & \texttt{Row major}: 9,8,4,2,5,2
\end{aligned}
$$

---
