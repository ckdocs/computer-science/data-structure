# Jagged Array

**Jagged Array** or **Array of Arrays** is a multidimensional array of varying sizes as its elements

![Jagged Array](../assets/jagged_array.png)

```cpp
int a[5][];
a[0] = new int[4];
a[1] = new int[3];
a[2] = new int[13];
a[3] = new int[5];
a[4] = new int[9];

a[3][3] = 123;
```

---
