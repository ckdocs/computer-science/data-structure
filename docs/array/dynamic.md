# Dynamic Array

**Dynamic Array** or **Array List** is a type of **Static Array** that size can **Increase** when full or **Decrease** when empty

![Dynamic Array](../assets/dynamic_array.png)

---

## Concepts

There are many important concepts in **Dynamic Array**

---

### Growth factor

For **Performance Optimization** of dynamic arrays, when it fills up it size will change by a **Growth Factor**

-   **Example**: Growth Factor is `2`, when array is full size `doubled`
    ![Growth Factor](../assets/dynamic_array_growth.png)

---

## Add

-   **Add** like `static array`
-   **Increase** size by `growth factor` if full

```cpp
float growthFactor = 1.5;
int top = 5;
int size = 10;
int array[size] = {1,2,3,4,5,0,0,0,0,0};

void add(int index, int value) {
    // Add like static array
    rightShift(array[index]);
    array[index] = value;
    top++;

    // Increase size
    if (top == size) {
        size = size * growthFactor;
        array = realloc(array, size);
    }
}
```

---

## Remove

-   **Remove** like `static array`
-   **Decrease** size by `inverse growth factor` if empty

```cpp
float growthFactor = 1.5;
int top = 5;
int size = 10;
int array[size] = {1,2,3,4,5,0,0,0,0,0};

void remove(int index) {
    // Remove like static array
    top--;
    array[index] = 0;
    leftShift(array[index]);

    // Decrease size
    if (top < size / growthFactor) {
        size = size / growthFactor;
        array = realloc(array, size);
    }
}
```

---
