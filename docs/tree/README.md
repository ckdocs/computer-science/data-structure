# Tree (Rooted Tree)

Is a special category of **Graphs** with these features:

1. **Connected**
2. **Without Cycle**

There are many concepts in **Rooted Trees**:

![Concepts](../assets/tree_concepts.jpg)

1. **Key**: `Value` of a node
2. **Root**: The `top-most` node
3. **Parent**: `Upward node` of another node
4. **Children**: `Downward nodes` of another node
5. **Sibling (Brothers)**: `Other childs` of a node `parent`
6. **Ancestor (Super nodes)**: All `related fathers` of a node
7. **Descendent (Sub nodes)**: All `related children` of a node
8. **Leaf (External Node)**: A node `without` children
9. **Simple (Internal Node)**: A node with `one or many` children
10. **Branch**: A path from `root` to a `node`
11. **Degree**: `Number of children` of a node
12. **Max Degree (K-ary)**: The `maximum number of children` in each node
13. **Sub-Tree**: A `node` and it's `descendants`
14. **Depth (Level)**: Number of `edges` from the `root` to the `node`
    - **Tree Depth**: Number of `edges` from the `root` to the `deepest leaf`
15. **Height**: Number of `edges` from the `deepest leaf` to the `node` (**Longest path**)
    - **Tree Height**: Number of `edges` from the `deepest leaf` to the `root`

![Tree Depths Heights](../assets/tree_depths_heights.png)

---

## Categories

Based on the **tree root** we have two categories of trees:

1. **Free Tree**: `Graph`
    - A **Connected** graph **Without Cycle**
2. **Rooted Tree**: `Graph` + `Root`
    - A **Free Tree** with concepts of **Root** and **Parent** and **Child**
    - Each **Sub-Tree** of a **Rooted Tree** is also a **Rooted Tree**

![Free Rooted](../assets/tree_free_rooted.png)

---

Based on the **number of children** we have two categories of trees:

1. **Binary Tree**: Each node can have `0` or `1` or `2` children
2. **General Tree**: Each node can have `any` number of children

---

Based on the **usage** we have two categories of trees:

1. **Search Tree**: Optimized for `search` operations
2. **Heap Tree**: Optimized for `min/max` operations

---

## Pathological, Skewed

-   A **pathological** or **degenerate** tree, is the tree having a **single child** either left or right.
-   A **skewed** tree, is a `pathological` tree having only **left child** or **right child**.
    -   There are two types of skewed trees:
        -   **Left Skewed Tree**: Nodes with only left child
        -   **Right Skewed Tree**: Nodes with only right child

![Pathological, Skewed](../assets/tree_pathological_skewed.png)

---

## Full, Complete, Perfect

Based on the **arrangement** of nodes on the levels we have three important types of **K-ary Trees**:

-   **Full Tree** (**0 or 2**): Each node is `Empty` or `Full` of children
-   **Complete Tree** (**Left to Right**): Nodes fill from `Left-to-Right` in layers
-   **Perfect Tree** (**Full and Complete**): Each node is `Full` of children

![Full, Complete, Perfect](../assets/tree_full_complete_perfect.jpg)

---

## Balanced, Non-Balanced

-   A tree with **Balance Factor** equals to **-1 or 0 or 1** called `Balanced`.
-   We must **Reduce** the height of tree, to optimize operations from $O(n)$ to $O(\log{n})$:

![Balanced Non-Balanced](../assets/tree_balanced_nonbalanced.jpg)

So we will introduce special category of trees implemented using **Binary Trees** and **General Trees**:

1. **Search**: Optimized for **searching** in $O(\log{n})$
2. **Heap**: Optimized for **finding min,max** in $O(1)$

---

### Balance Factor

**Difference** between **height** of **left sub-tree** and **right sub-tree** in a node

![Balance Factor](../assets/tree_balance_factor.png)

$$
\begin{aligned}
    & BF(node) = height(node.left) - height(node.right)
\end{aligned}
$$

---

## Orders

![Search Tree Orders](../assets/search_tree_orders.png)

![Heap Tree Orders](../assets/heap_tree_orders.png)

---
