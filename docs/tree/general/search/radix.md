# Radix

**Radix Tree** is a **Memory Optimized** version of **Trie** where **Single Edge** pathes will merge into one **Edge**

![Radix](../../../assets/radix.png)

---
