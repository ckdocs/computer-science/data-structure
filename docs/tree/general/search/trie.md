# Trie

**Trie** or **Retrieval Tree** or **Digital Tree** or **Prefix Tree** is a type of search tree for locating **String** keys like **Dictionary**

The properties of this tree are:

1.  Trie is a tree where **edges** are labelled with **characters**
2.  Each **node** in the tree **represents** a string of **path**
3.  Double circled nodes are contains **string**

![Trie](../../../assets/trie.png)

-   **Tip**: Trie used to **Dictionary Sort** elements (Radix Sort)
-   **Tip**: In Trie we store a boolean `isData` in each node:

```ts
class TrieNode {
    isData: boolean;

    children: TrieNode[];
}
```

---

## Concepts

There are many important concepts in **Trie**

---

### Traverse

By traversing a **Trie** with **Pre-Order** of **Edges**, we get sorted words **Ascending**:

![Traverse](../../../assets/trie.png)

$$
\begin{aligned}
    & cat \leq cats \leq cow \leq pi \leq pig \leq pin
\end{aligned}
$$

-   **Time Complexity**: $O(b)$ where `b` is the number of all characters in all words

---

### Height

The `height` of the tree is the **Maximum length of words**

---
