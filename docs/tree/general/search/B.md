# B

**B Tree** or **Balanced Tree** is type of **M-Way Search Tree** that is balanced and optimized the M-Way Search Tree operations from **O(n)** to **O(logn)**

The properties of this tree are:

1. It's **M-Way Search Tree**
2. Each tree has an **Order** named `m`
3. Each tree has a **Degree** named `t` (`t = m/2`)
    - The **Root** node has `range` of **[1, 2t-1]** keys
    - Each **Leaf** node has `range` of **[t-1, 2t-1]** keys
    - Each **Internal** node has `range` of **[t-1, 2t-1]** keys
4. Each **Internal Node** (`Not leaf`) with **x** keys contains **x+1** children

![B](../../../assets/b.png)

---

## Concepts

There are many important concepts in **B-Tree**:

1. Used in database indexing to speed up the search.
2. Used in file systems to implement directories.

---

### Min, Max topologies

There are two main topologies:

-   **Fat** tree:
    -   **Root** node:
        -   Has **2t** children
        -   Has **2t-1** keys
    -   **Internal** node:
        -   Has **2t** children
        -   Has **2t-1** keys
    -   **Leaf** node:
        -   Has **0** children
        -   Has **2t-1** keys
    -   `Maximum` number of **keys**
    -   `Maximum` number of **nodes**
    -   `Minimum` number of **levels** (height)
    -   $$
        \begin{aligned}
            & N(h) = 1 + (2t) + \dots + (2t)^{h-1}
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
            \\
            \\ \implies
            & N(h) = (2t)^h - 1
            \\ \implies
            & h = \log_{2t}^{(N(h) + 1)}
            \\ \implies
            & h = \lfloor \log_{2t}^{N(h)} \rfloor + 1
        \end{aligned}
        $$
-   **Slim** tree:
    -   **Root** node:
        -   Has **2** children
        -   Has **1** keys
    -   **Internal** node:
        -   Has **t** children
        -   Has **t-1** keys
    -   **Leaf** node:
        -   Has **0** children
        -   Has **t-1** keys
    -   `Minimum` number of **keys**
    -   `Minimum` number of **nodes**
    -   `Maximum` number of **levels** (height)
    -   $$
        \begin{aligned}
            & N(h) = 1 + 2.(1 + t + \dots + t^{h-1})
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
            \\
            \\ \implies
            & N(h) = 1 + 2.(t^h - 1)
            \\ \implies
            & N(h) = 2.(t^h) - 1
            \\
            & h = \log_{t}^{\frac{(N(h) + 1)}{2}}
            \\ \implies
            & h = \lfloor \log_{t}^{\frac{N(h)}{2}} \rfloor + 1
        \end{aligned}
        $$

![Topologies](../../../assets/b_topologies.png)

$$
\begin{aligned}
    & \lfloor \log_{2t}{n} \rfloor + 1 \lt h \lt \lfloor \log_{t}{\frac{n}{2}} \rfloor + 1
\end{aligned}
$$

---

## Add

-   Add like **M-Way Search Tree**
-   Split **Node** if it's full
    -   **Bottom-Up**: Split from founded leaf to root
    -   **Top-Down**: Split full nodes when moving from root to leaf

![Add](../../../assets/b_add.png)

-   **Tip**: Each `Split` operation will increase the `tree height` by `one level`

---

## Remove

---
