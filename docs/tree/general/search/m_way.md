# M-Way

**M-Way Search Tree** or **Multi Way Search Tree** is a generalized version of **BST** where each node contains multiple elements

The properties of this tree are:

1. Keys are stored in **Increasing** order
2. Each tree has an **Order** named `m`
3. Each node has `maximum` of **m-1** keys
4. Each node has `maximum` of **m** children
5. Each `key` in each node is between **Two sub-trees**:
    - **Nearest** `left` child
    - **Nearest** `right` child

![M-Way](../../../assets/m_way.png)

![Node](../../../assets/m_way_node.png)

```ts
const m = 4;

class MWayNode {
    keys: number[m - 1];
    children: MWayNode[m];
}
```

-   **Tip**: By traversing a **M-Way Search Tree** with **In-Order**, we get sorted elements **Ascending**:

    $$
    \begin{aligned}
        & 7, 8, 10, 12, 18, 44, 76, 77, 80, 92, 141, 148, 151, 172, 186, 198, 262, 272, 286, 350
    \end{aligned}
    $$

---

## Concepts

There are many important concepts in **M-Way Search Tree**

---

## Search

-   If node is **NULL** or **Contains Key** return `node`
-   Search over **Keys** and go to `closest child`

![Search](../../../assets/m_way_search.png)

$$
\begin{aligned}
    & \texttt{Search(31)}: MWayNode\{\dots\}
\end{aligned}
$$

```ts
const search = (head: MWayNode, key: number) => {
    if (head === null || head.keys.includes(key)) {
        return head;
    }

    // Tree degree
    const m = 5;

    // Search over keys and go to the closest child
    for (let i = 0; i < m; i++) {
        if (head.keys[i] > key) {
            return search(head.children[i], key);
        }
    }

    return search(head.children[m], key);
};
```

-   **Tip**: There are two ways to search in `keys`:
    -   **Linear Search**: $O(m)$
    -   **Binary Search**: $O(\log{m})$
-   **Tip**: The complexity of searching depends on `key search` algorithm and `height` of tree:
    -   **Linear Key Search**: $O(m \times h)$
    -   **Binary Key Search**: $O(\log{m} \times h)$
-   **Tip**: For balanced trees, the height is $\log_m^n$ and complexities are:
    -   **Linear Key Search**: $O(m \times \log_m^n)$
    -   **Binary Key Search**: $O(\log{m} \times \log_m^n) = O(\log{n})$
-   **Tip**: Complexities of **Balanced M-Way Search Tree** and **Balanced Binary Search Tree** are equals to $O(\log{n})$

---
