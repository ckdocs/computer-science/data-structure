# Forest

One or many number of **General Trees** called **Forest**

-   Forest is a **General Tree** that removed it's root (Has many sub-trees)

![Forest](../../assets/forest.png)

---

## Concepts

---

### Traversing

Also we can define **DFS** traversing rules for **Forest**:

-   **Pre-Order**: `Pre-Order` of trees from **Left-to-Right**
-   **Post-Order**: `Post-Order` of trees from **Left-to-Right**

---

-   **Example**: Find the traversing of this forest:

![Forest](../../assets/forest.png)

-   **Pre-Order**: BDEIJF-CGKH
-   **Post-Order**: DIJEFB-KGHC

---

## Left-Child/Right-Sibling Forest

The way of storing a **forest** in a **Binary Tree** format:

1. **Left Link**: Left most child
2. **Right Link**: Right sibling

For converting a **Forest** to **Binary Tree**:

1. **Connect** `siblings` to each other
2. **Connect** `left-most child` to parent
3. **Disconnect** other `children` from parent

```ts
class GeneralNode {
    data: number;
    leftChild: GeneralNode;
    rightSibling: GeneralNode;
}
```

-   **Tip**: Traversing **Binary Tree** of a **Forest** will shows a different result:
    -   **Pre-Order** of `Left-Child/Right-Sibling` $\implies$ **Pre-Order** of `Forest`
    -   **In-Order** of `Left-Child/Right-Sibling` $\implies$ **Post-Order** of `Forest`

![Left-Child Right-Sibling Forest](../../assets/left_child_right_sibling_forest.png)

---
