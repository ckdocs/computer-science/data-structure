# Fibonacci Heap

**Fibonacci Heap Tree** is type of **Binomial Heap** that can contains any number of same $B_k$ binomial trees

![Fibonacci Heap](../../../assets/fibonacci_heap.png)

-   **Tip**: Fibonacci Heap optimizes the **Binomial Min Heap** operations in **Amortized** from $\Theta(\log{n})$ to $\Theta(1)$ except **Remove** operations

---
