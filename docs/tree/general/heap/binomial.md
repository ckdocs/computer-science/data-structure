# Binomial Heap

**Binomial Heap Tree** is a set of **Binomial Trees** where each Binomial Tree follows `min heap property`. And there can be `at most one` Binomial Tree of any degree.

The properties of this tree are:

1. A set of **Binomial Trees**
2. Each tree is **Min Heap**
3. At **Most One Tree** of each `degree`
4. Trees are sorted **Ascending**
5. Tree **Roots** are **Linked**

![Binomial Heap](../../../assets/binomial_heap.png)

-   **Tip**: Binomial Heap, it's an extension over **Binary Heap** to optomize the `merge` and `union` operations
-   **Tip**: We must save the **Min Node** pointer to optimize `min` operation to $O(1)$

---

## Concepts

There are many important concepts in **Binomial Heap**

---

### Binomial tree

A binomial tree of order **k** contains two binomial trees of order **k-1** that `roots are linked`, a binomial tree of order **0** contains `one node`:

-   $B_k = B_{k-1} + B_{k-1}$
-   $B_k = B_{k-1} + B_{k-2} + \dots + B_1 + B_0$

![Binomial Tree](../../../assets/binomial_tree.jpg)

-   **Number of nodes**: $2^k$
-   **Degree of nodes**: $k$
-   **Height of tree**: $k$
-   **Number of nodes in depth `i`**: ${k \choose i}$

---

### Number of trees

Number of possible trees generates with **n** nodes:

1. **Tree Topologies** with **n** keys: There are only **One** `binomial heap tree` exists with `n` keys:
    - $$
      \begin{aligned}
          & \texttt{Tree Topologies}: 1
      \end{aligned}
      $$
2. **Binomial Trees** with **n** keys: By converting the `n` into `binary format` we can find the **Binomial Trees** exists in the **Binomial Heap**:
    - $$
      \begin{aligned}
          & n = 2^i + 2^j + \dots + 2^t
          \\
          \\ \implies
          & \texttt{Binomial Heap}: B_i + B_j + \dots + B_t
      \end{aligned}
      $$

---

-   **Example**: Draw a binomial heap with **13** nodes

$$
\begin{aligned}
    & 13 = 8 + 4 + 1
    \\
    & = 2^3 + 2^2 + 2^0
    \\
    & = B_3 + B_2 + B_0
\end{aligned}
$$

![Heaps](../../../assets/binomial_heaps.png)

---

### Min, Max topologies

There are two main topologies:

-   **Fat** tree:
    -   Heap is contains **All** `binomial trees`
    -   `Maximum` number of **roots** ($\log{n}$)
    -   $$
        \begin{aligned}
            & N(x) = 2^0 + 2^1 + \dots + 2^{k-1}
            \\
            & \textbf{N(x)}: \texttt{Number of nodes}
            \\
            & \texttt{Binomial Heap}: B_0 + B_1 + \dots + B_{k-1}
        \end{aligned}
        $$
-   **Slim** tree:
    -   Heap contains only **Biggest** `binomial tree`
    -   `Minimum` number of **roots** ($1$)
    -   $$
        \begin{aligned}
            & N(x) = 2^k
            \\
            & \textbf{N(x)}: \texttt{Number of nodes}
            \\
            & \texttt{Binomial Heap}: B_k
        \end{aligned}
        $$

$$
\begin{aligned}
    & 1 \lt h \lt \log{n}
    \\
    & 1 \lt r \lt \log{n}
    \\
    \\
    & \textbf{h}: \texttt{Maximum height}
    \\
    & \textbf{r}: \texttt{Maximum number of roots}
\end{aligned}
$$

---

## Min, Max

-   **Minimum**: Search over `roots` in $O(\log{n})$
    -   By saving the `Min` pointer we can optimize it to $O(1)$
-   **Maximum**: Search over `nodes` in $O(n)$

![Min Max](../../../assets/binomial_heap_min_max.jpg)

```ts
const min = (heap: number[]) => {
    // Search over roots
};

const max = (heap: number[]) => {
    // Search over nodes
};
```

---

## Add

-   **Merge** a $B_0$ into tree

![Add](../../../assets/binomial_heap_add.jpg)

---

## Extract

-   **Remove** minimum `Root`
-   **Merge** sub-trees intor tree

---

## Merge

-   Iterate over heaps **Roots** from **Smaller** to **Greater**: $O(\log{n})$
    -   Merge trees with same **Order**: ($B_k = B_{k-1} + B_{k-1}$)
    -   Link tree with **Bigger Root** to root of other tree

![Merge](../../../assets/binomial_heap_merge.png)

---
