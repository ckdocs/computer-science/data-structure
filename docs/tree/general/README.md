# General Tree (k-ary)

A type of **Rooted Tree** which each parent node can have `any number` of child nodes

![General Tree](../../assets/general_tree.png)

---

## Concepts

There are many important concepts in **General Tree**

---

### Left, Right

In general tree we have not the concept of **Left Child** and **Right Child** so both are same (Number of childs is important)

![General Tree Rotations](../../assets/general_tree_rotations.png)

---

### Traversing

Also we can define **DFS** traversing rules for **General Tree**:

![Traversing](../../assets/general_tree_traversing.png)

-   **Pre-Order** $\implies r T_1 T_2 \dots T_k$
-   **In-Order** $\implies T_1 r T_2 \dots T_k$
-   **Post-Order** $\implies T_1 T_2 \dots T_k r$

---

## Dynamic Node Tree

The way of storing a **general tree** using a **Dynamic Array** or **Linked List**:

```ts
class GeneralNode {
    data: number;
    links: GeneralNode[];
}
```

-   **Tip**: We allocate a **Dynamic List** in each node
-   **Tip**: Another version of this implementation, is that each node has a **Fixed** number of links, used for **k-ary** trees

![List General Tree](../../assets/list_general_tree.png)

---

## Left-Child/Right-Sibling Tree

The way of storing a **general tree** in a **Binary Tree** format:

1. **Left Link**: Left most child
2. **Right Link**: Right sibling

For converting a **General Tree** to **Binary Tree**:

1. **Connect** `siblings` to each other
2. **Connect** `left-most child` to parent
3. **Disconnect** other `children` from parent

```ts
class GeneralNode {
    data: number;
    leftChild: GeneralNode;
    rightSibling: GeneralNode;
}
```

-   **Tip**: Number of **NULL** links for **n** nodes is **n+1**
-   **Tip**: Traversing **Binary Tree** of a **General Tree** will shows a different result:
    -   **Pre-Order** of `Left-Child/Right-Sibling` $\implies$ **Pre-Order** of `General Tree`
    -   **In-Order** of `Left-Child/Right-Sibling` $\implies$ **Post-Order** of `General Tree`

![Left-Child Right-Sibling Tree](../../assets/left_child_right_sibling_tree.png)

---
