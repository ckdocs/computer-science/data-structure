# Red-Black

**Red-Black Tree** is type of **BST** that is balanced and optimized the BST operations from **O(n)** to **O(logn)**

The properties of this tree are:

1. It's **BST** with some properties
    - **Color Property** (Red/Black): Each node is `red` or `black`
    - **Root Property** (Black root): The `root` is `black`.
    - **Leaf Property** (Black null): Every `leaf (NIL)` is `black`.
    - **Red Property** (No two red): If a `red node` has `children` then, the children are `always black`.
    - **Depth Property** (Equal black height): For each node, `left black height` is equals to `right black height`.
2. $\frac{height(left)}{height(right)}$ is in range **[1/2, 2]**
    - Height of a sub-tree **at last can be double** of other side height

![RB](../../../assets/rb.png)

-   **Tip**: In Red-Black tree we store the `color` bit (red or black) in each node

```ts
class RBNode {
    key: number;
    isRed: boolean;

    left: RBNode;
    right: RBNode;
    parent: RBNode;
}
```

---

## Concepts

There are many important concepts in **Red-Black Tree**

---

### Black height

![Black Height](../../../assets/rb_black_height.jpg)

---

### Colorize tree

-   **Root** is `black`
-   **Leafs** (NULL nodes) are `black`
-   **Shortest Path** is `black`
-   Make some `red` nodes in **Longer Pathes**

---

-   **Tip**: We can only colorize trees with this property:
    -   In each node **A Sub-Tree** height (With **NULL** edge) can be **2** of **Other Side**
-   **Tip**: A **Perfect** tree can colorize with **Only Black** (Without **Red**)
-   **Tip**: The **Mimimum** number of **Reds** can be found in **Perfect** trees
-   **Tip**: The **Maximum** number of **Reds** can be found in **Skewed Red-Black** trees

![Skewed](../../../assets/rb_skewed.png)

---

-   **Example**: What is the number of ways to colorize a perfect tree with **7** nodes: `5 ways`

![Colorize](../../../assets/rb_colorize.png)

---

### Min, Max topologies

There are two main topologies:

-   **Fat** tree:
    -   Tree is **complete**
    -   `Maximum` number of **nodes**
    -   `Minimum` number of **levels** (height)
    -   $$
        \begin{aligned}
            & N(h) = 1 + 2 + \dots + 2^{h-1}
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
            \\
            \\ \implies
            & N(h) = 2^h - 1
            \\ \implies
            & h = \log{(N(h) + 1)}
            \\ \implies
            & h = \lfloor \log{N(h)} \rfloor + 1
        \end{aligned}
        $$
-   **Slim** tree:
    -   Tree is **Red-Black skewed**
    -   `Minimum` number of **nodes**
    -   `Maximum` number of **levels** (height)
    -   By adding numbers **Ascending** or **Descending** we can create `longest` tree
    -   $$
        \begin{aligned}
            & N(h) = \sqrt{2^h} + 1
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
            \\
            \\ \implies
            & (N(h) - 1)^2 = 2^h
            \\ \implies
            & h = 2.\log{(N(h) + 1)}
        \end{aligned}
        $$

![Topologies](../../../assets/rb_topologies.png)

$$
\begin{aligned}
    & \log{(n + 1)} \lt h \lt 2.\log{(n + 1)}
\end{aligned}
$$

-   **Tip**: We can find the **Min** and **Max** number of nodes with **bh** `black height`:
    -   **Min node**: When tree is **perfect** `without red` level (**bh levels**)
    -   **Max node**: When tree is **perfect** with levels `one in red and black` (**2bh levels**)

![Min Max Black Height](../../../assets/rb_min_max_black_height.png)

$$
\begin{aligned}
    & min(n): 2^0 + 2^1 + \dots + 2^{bh-1} = 2^{bh} - 1
    \\
    & max(n): 2^0 + 2^1 + \dots + 2^{2.bh-1} = 2^{2.bh} - 1
    \\
    \\ \implies
    & 2^{bh} - 1 \lt n \lt 2^{2.bh} - 1
\end{aligned}
$$

---

## States

---

## Add

-   Add like **BST**
-   Set new node **Red**
-   If parent is **Black**: (Ok)
    -   `Return`
-   If parent is **NULL**: (`Root` node)
    -   Set color **Black**
    -   `Return`
-   If parent is **Red**:
    -   If State is **LLr** or **LRr** or **RLr** or **RRr**:
        -   `Recolor`:
            -   Set **Parent**, **Uncle** `black`
            -   Set **Grand Parent** `red`
        -   `fix(G)`
    -   If State is **LRb** or **RLb**:
        -   `Rotate`: (To be `skewed`)
            -   If **LRb**: `left(P)`
            -   If **RLb**: `right(P)`
        -   `fix(P)`
    -   If State is **LLb** or **RRb**:
        -   `Recolor`:
            -   Set **Parent**, **Uncle** `black`
            -   Set **Grand Parent** `red`
        -   `Rotate`: (To be `balanced`)
            -   If **LLb**: `right(G)`
            -   If **RRb**: `left(G)`

![Add](../../../assets/rb_add.png)

```ts
const add = (head: RBNode, key: number) => {
    // Iteratively find the node
    let parent;
    let target = head;
    while (target !== null) {
        parent = target;
        if (key < target.key) {
            target = target.left;
        }
        if (key > target.key) {
            target = target.right;
        }
    }

    // Create new node
    let node = new RBNode(key, "RED");
    node.left = null;
    node.right = null;
    node.parent = parent;
    if (node.key < node.parent.key) {
        node.parent.left = node;
    } else {
        node.parent.right = node;
    }

    // New node is root (first item)
    if (node.parent === null) {
        node.color = "BLACK";
        return node;
    }

    // Balance tree after insert
    fixAdd(node);

    return head;
};

const fixAdd = (node: RBNode) => {
    // LLr, LRr, RLr, RRr
    if (
        node.isRed &&
        node.parent.isRed &&
        uncle(node.parent).isRed &&
        !node.parent.parent.isRed
    ) {
        // Recolor
        // Fix
    }

    // LRb, RLb
    if (...) {
        // Rotate
        // Fix
    }

    // LLb, RRb
    if (...) {
        // Recolor
        // Rotate
    }
};
```

-   **Tip**: The `fix` function will `recursively call` at most **3** times
-   **Tip**: Each `insert` will `rotate` at most **2** times

---

## Remove

---
