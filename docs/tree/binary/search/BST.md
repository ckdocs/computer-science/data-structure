# BST

**Binary Search Tree** or **Sorted Binary Tree** or **Ordered Binary Tree** is type of **Binary Tree** optimized for searching operations, that store elements sorted

The properties of this tree are:

1. **Parent** is greater that all nodes of **Left sub-tree**
2. **Parent** is less that all nodes of **Right sub-tree**

![BST](../../../assets/bst.png)

-   **Tip**: By traversing a **BST** with **In-Order**, we get sorted elements **Ascending**:

    $$
    \begin{aligned}
        & 3, 11, 16, 22, 30, 40, 56, 60, 63, 65, 67, 70, 95
    \end{aligned}
    $$

---

## Concepts

There are many important concepts in **BST**

---

-   **Example**: Check this pre-order traversation is valid for a BST or not?

$$
\begin{aligned}
    & \texttt{PreOrder}: 12, 8, 5, 10, 9, 20, 14, 15, 18, 13, 16, 19
    \\
    \\ \implies
    & (12), (8, 5, 10, 9), (20, 14, 15, 18, 13, 16, 19)
    \\ \implies
    & (12), ((8), (5), (10, 9)), (20, 14, 15, 18, 13, 16, 19)
    \\ \implies
    & (12), ((8), (5), ((10), (9))), (20, 14, 15, 18, 13, 16, 19)
    \\ \implies
    & (12), ((8), (5), ((10), (9))), ((20), (14, 15, 18, 13, 16, 19))
    \\ \implies
    & (12), ((8), (5), ((10), (9))), ((20), ((14), [15, 18, \textbf{13}, 16, 19]))
    \\ \implies
    & \texttt{It cannot be a BST}
\end{aligned}
$$

---

### Number of trees

Number of possible trees generates with **n** nodes:

1. **Tree Topologies** with **n** keys: Possible binary trees
    - $$
      \begin{aligned}
          & \texttt{Tree Topologies}: C_n = \sum_{i=1}^{n} C_{k-1} . C_{n-k} = \frac{1}{n + 1}{2n \choose n}
      \end{aligned}
      $$
2. **Key Combinations** with **n** keys: Each topology has one possible combination when **No Duplication Keys**
    - $$
      \begin{aligned}
          & \texttt{Key Combinations}: 1
      \end{aligned}
      $$

---

-   **Example**: What is the number of **Possible BST Trees** with **1,2,3**?

$$
\begin{aligned}
    & C_3 = \frac{1}{4}.{6 \choose 3} = \frac{1}{4}.\frac{6 \times 5 \times 4}{3 \times 2 \times 1} = 5
\end{aligned}
$$

![Trees](../../../assets/bst_trees.png)

---

### Min, Max topologies

There are two main topologies:

-   **Fat** tree:
    -   Tree is **complete**
    -   `Maximum` number of **nodes**
    -   `Minimum` number of **levels** (height)
    -   $$
        \begin{aligned}
            & N(h) = 1 + 2 + \dots + 2^{h-1}
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
            \\
            \\ \implies
            & N(h) = 2^h - 1
            \\ \implies
            & h = \log{(N(h) + 1)}
            \\ \implies
            & h = \lfloor \log{N(h)} \rfloor + 1
        \end{aligned}
        $$
-   **Slim** tree:
    -   Tree is **skewed**
    -   `Minimum` number of **nodes**
    -   `Maximum` number of **levels** (height)
    -   By adding numbers **Ascending** or **Descending** we can create `longest` tree
    -   $$
        \begin{aligned}
            & N(h) = h
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
        \end{aligned}
        $$

![Topologies](../../../assets/bst_topologies.png)

$$
\begin{aligned}
    & \lfloor \log{n} \rfloor + 1 \lt h \lt n
\end{aligned}
$$

---

## Validate

We can validate a **BST** using it's **DFS Traversations** in $O(n)$:

-   **PreOrder DFS**: `rAB`
    -   `A` is a sequence `smaller` than `r`
    -   `B` is a sequence `greater` than `r`
-   **PostOrder DFS**: `ABr`
    -   `A` is a sequence `smaller` than `r`
    -   `B` is a sequence `greater` than `r`
-   **InOrder DFS**: `ArB`
    -   `A` is a sequence `smaller` than `r`
    -   `B` is a sequence `greater` than `r`
    -   Check **InOrder DFS** of a **BST** is **Increasing**

![Validate](../../../assets/bst_validate.png)

$$
\begin{aligned}
    & \texttt{InOrder DFS First}: 1, 3, 4, 6, 8, 10
    \\
    & \texttt{InOrder DFS Second}: 1, 3, 2, 6, 8, 10
\end{aligned}
$$

```ts
const validate = (head: BinaryNode) => {
    const sequence = inorderDFS(head);

    return isIncreasing(sequence);
};
```

---

## Search

-   If node is **NULL** or **Equal** return `node`
-   If node is **Greater** go to `left`
-   If node is **Smaller** go to `right`

![Search](../../../assets/bst_search.png)

$$
\begin{aligned}
    & \texttt{Search(6)}: Node\{\dots\}
\end{aligned}
$$

```ts
const search = (head: BinaryNode, key: number) => {
    if (head === null || key === head.key) {
        return head;
    }

    if (key < head.key) {
        return search(head.left, key);
    }

    if (key > head.key) {
        return search(head.right, key);
    }
};
```

---

## Min, Max

-   **Minimum**: `left most` child of `root`
-   **Maximum**: `right most` child of `root`

![Min Max](../../../assets/bst_min_max.png)

$$
\begin{aligned}
    & \texttt{Min}: 1
    \\
    & \texttt{Min}: 10
\end{aligned}
$$

```ts
const min = (head: BinaryNode) => {
    while (head.left !== null) {
        head = head.left;
    }

    return head.key;
};

const max = (head: BinaryNode) => {
    while (head.right !== null) {
        head = head.right;
    }

    return head.key;
};
```

---

## Succ, Pred

-   **Successor (Before)**:
    -   If left isn't null return **max(left)**
    -   Else return **first left ancestor**
-   **Predecessor (After)**:
    -   If right isn't null return **min(right)**
    -   Else return **first right ancestor**

![Succ Pred](../../../assets/bst_succ_pred.png)

$$
\begin{aligned}
    & \texttt{InOrder BFS}: 1, 3, 4, 6, 7, 8, 10, 13, 14
    \\
    \\
    & \texttt{Succ(8)}: Max(\texttt{Left}) = 7
    \\
    & \texttt{Pred(8)}: Min(\texttt{Right}) = 10
    \\
    & \texttt{Succ(7)}: Parent(\texttt{I'm right}) = 6
    \\
    & \texttt{Pred(7)}: Parent(\texttt{I'm left}) = 8
\end{aligned}
$$

```ts
const succ = (node: BinaryNode) => {
    if (node.left !== null) {
        return max(node.left);
    }

    let parent = node.parent;
    while (parent !== null) {
        if (parent.right === node) {
            break;
        }

        node = parent;
        parent = parent.parent;
    }

    return parent.key;
};

const pred = (head: BinaryNode) => {
    if (node.right !== null) {
        return min(node.right);
    }

    let parent = node.parent;
    while (parent !== null) {
        if (parent.left === node) {
            break;
        }

        node = parent;
        parent = parent.parent;
    }

    return parent.key;
};
```

-   **Tip**: Another way of finding **succ, pred** is:
    -   Traverse tree **In-Order DFS**
    -   **Successor** is the `next` element of node
    -   **Predecessor** is the `previous` element of node

---

## Rotate

Is an operation for **Rebalancing** a binary tree `without changing` the **BST** property

![Rotation](../../../assets/rotation.gif)

There are two `basic` types of rotations:

1. **Left Rotate**: Rebalancing a **Right Skewed** tree
2. **Right Rotate**: Rebalancing a **Left Skewed** tree

There are two `complex` types of rotations:

1. **Left-Right Rotate**: Rebalancing a **Left Arched** tree
2. **Right-Left Rotate**: Rebalancing a **Right Arched** tree

---

-   **Tip**: Complexity of rotations are **O(1)**
-   **Tip**: In `left` or `right` rotations **3** pointers will change
    1. `right(left(x))`
    2. `parent(x)`
    3. `left(x)`

---

-   **Tip**: Rotation operations will not change **In-Order DFS** (BST is `fixed`)
-   **Tip**: We can convert any `topology` to any other `topology` with **O(n)** rotations
-   **Tip**: We can convert `left skewed` to `right skewed` or reverse in:
    -   **Min**: $n$ rotations (**From up**)
    -   **Max**: $\frac{n.(n-1)}{2}$ rotations (**From bottom**)

---

-   **Tip**: We can `undo` a rotation using `reverse` over **Child**
-   **Warning**: Do **not** rotate over **same node** !

![Rotation Undo](../../../assets/rotation_undo.png)

---

### Left Rotation

-   Set **New Root** to **Old Root Right**
-   Set **Old Root Right** to **New Root Left**
-   Set **New Root Left** to **Old Root**
-   Update **Heights**

![Left Rotation](../../../assets/left_rotation.jpg)

```ts
const leftRotate = (node: AVLNode) => {
    const right = node.right;

    // Perform rotation
    node.right = right.left;
    right.left = node;

    // Update heights
    node.height = Math.max(node.left.height, node.right.height) + 1;
    right.height = Math.max(right.left.height, right.right.height) + 1;

    // Return new root
    return right;
};
```

---

### Right Rotation

-   Set **New Root** to **Old Root Left**
-   Set **Old Root Left** to **New Root Right**
-   Set **New Root Right** to **Old Root**
-   Update **New Root** and **New Root** heights

![Right Rotation](../../../assets/right_rotation.jpg)

```ts
const rightRotate = (node: AVLNode) => {
    const left = node.left;

    // Perform rotation
    node.left = left.right;
    left.right = node;

    // Update heights
    node.height = Math.max(node.left.height, node.right.height) + 1;
    left.height = Math.max(left.left.height, left.right.height) + 1;

    // Return new root
    return left;
};
```

---

### Left-Right Rotation

![Left-Right Rotation](../../../assets/left_right_rotation.png)

---

### Right-Left Rotation

![Right-Left Rotation](../../../assets/right_left_rotation.png)

---

## Create

There are two ways of creating a tree:

-   **Bulk Create**: Create a `BST` from a `DFS Traversation` in $O(n)$
    -   **PreOrder DFS**: `rAB`
        -   TODO
    -   **PostOrder DFS**: `ABr`
        -   TODO
    -   **InOrder DFS**: `ArB`: $O(n + n.\log{n}) = O(n.\log{n})$
        -   **Sort** items accending (To get **InOrder DFS**): $O(n.\log{n})$
        -   **Create** new node from `median`
        -   **Bulk Create** for `left items`
        -   **Bulk Create** for `right items`
-   **Add Create**: Create a `BST` by `Adding Keys To Empty BST` in $O(n.\log{n})$
    -   **Add** each item to an empty tree

```ts
const create = (items: number) => {
    // O(n.logn)
    const sortedItems = sort(items);

    // O(n)
    return createFromInOrder(sortedItems);
};

const createFromInOrder = (items: number[]) => {
    const median = sortedItems[items / 2];
    const leftItems = sortedItems.splice(0, items / 2 - 1);
    const rightItems = sortedItems.splice(items / 2 + 1, items);

    let head = new BinaryNode(median);
    head.left = create(leftItems);
    head.right = create(rightItems);

    return head;
};
```

-   **Tip**: Bulk create algorithm will create a **Balanced Binary Tree**

---

## Add

-   If node is **NULL** or **Equal** `create` and return `new node`
-   If node is **Greater** go to `left`
-   If node is **Smaller** go to `right`

![Add](../../../assets/bst_add.png)

$$
\begin{aligned}
    & \texttt{Add(4)}: Node\{\dots\}
\end{aligned}
$$

```ts
const add = (head: BinaryNode, key: number) => {
    // Recursively find the node
    if (head === null) {
        // Create node
        return new BinaryNode(key);
    }
    if (key < head.key) {
        head.left = add(head.left, key);
    }
    if (key > head.key) {
        head.right = add(head.right, key);
    }

    return head;
};
```

-   **Tip**: If node is `equals to key`, we can select the **Left or Right** to find the `successor` or `predecessor`
-   **Tip**: The order of adding is important, by changing the order the `tree topology` may change
-   **Tip**: Instead of saving duplicate keys we can save a **Count** in each node and increase it for duplications

---

## Remove

-   If node **Hasn't Children** `remove` it
-   If node **Has One Child** `replace` it's `child` with it
-   If node **Has Two Child** `replace` it's `successor` or `predecessor` with it

![Remove](../../../assets/bst_remove.png)

```ts
const remove = (head: BinaryNode, key: number) => {
    // Recursively find the node
    if (head === null) {
        // Node not found
        return head;
    }
    if (key < head.key) {
        head.left = remove(head.left, key);
    }
    if (key > head.key) {
        head.right = remove(head.right, key);
    }

    // Node found
    if (key === head.key) {
        // Replace right child
        if (node.left === null) {
            const child = node.right;
            delete node;
            return child;
        }

        // Replace left child
        if (node.right === null) {
            const child = node.left;
            delete node;
            return child;
        }

        // Replace succ or pred
        const pred = min(node.right);
        node.key = pred.key;
        node.right = remove(node.right, pred.key);
    }

    return head;
};
```

-   **Tip**: The order of removing is important, by changing the order the `tree topology` may change

---
