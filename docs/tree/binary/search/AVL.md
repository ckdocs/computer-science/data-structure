# AVL

**Adelson-Velsky and Landis Tree** or **Balanced BST** is type of **BST** that is balanced and optimized the BST operations from **O(n)** to **O(logn)**

The properties of this tree are:

1. It's **BST**
2. **Balance Factor** of each node is **-1 or 0 or 1**

![AVL](../../../assets/avl.png)

-   **Tip**: In AVL tree we store the `height` (instead of balance factor) in each node:

```ts
class AVLNode {
    key: number;
    height: number;

    left: AVLNode;
    right: AVLNode;
}
```

---

## Concepts

There are many important concepts in **AVL Tree**

---

### Number of trees

Number of possible trees generates with **n** nodes:

-   There isn't any formula for number of trees
-   We can only find them by hand
-   We can use **previous steps** and add one node to possible places (**Recursive Formula**)

| Nodes | Possible AVL Topologies |
| ----- | ----------------------- |
| 1     | 1                       |
| 2     | 2                       |
| 3     | 1                       |
| 4     | 4                       |
| 5     | 6                       |
| 6     | ?                       |
| 7     | 17                      |

---

-   **Example**: What is the number of **Possible AVL Trees** with **1,2,3,4**?

$$
\begin{aligned}
    & C_4 = 4
\end{aligned}
$$

![Trees](../../../assets/avl_trees.png)

---

### Min, Max topologies

There are two main topologies:

-   **Fat** tree:
    -   Tree is **complete**
    -   `Maximum` number of **nodes**
    -   `Minimum` number of **levels** (height)
    -   $$
        \begin{aligned}
            & N(h) = 1 + 2 + \dots + 2^{h-1}
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
            \\
            \\ \implies
            & N(h) = 2^h - 1
            \\ \implies
            & h = \log{(N(h) + 1)}
            \\ \implies
            & h = \lfloor \log{N(h)} \rfloor + 1
        \end{aligned}
        $$
-   **Slim** tree:
    -   Tree is **AVL skewed**
    -   `Minimum` number of **nodes**
    -   `Maximum` number of **levels** (height)
    -   By adding numbers **Ascending** or **Descending** we can create `longest` tree
    -   $$
        \begin{aligned}
            & N(h) = N(h-1) + N(h-2) + 1
            \\
            & N(0) = 1
            \\
            & N(1) = 2
            \\
            \\
            & \textbf{N(h)}: \texttt{Number of nodes}
            \\
            & \textbf{h}: \texttt{Number of levels}
        \end{aligned}
        $$

![Topologies](../../../assets/avl_topologies.png)

$$
\begin{aligned}
    & \lfloor \log{n} \rfloor + 1 \lt h \lt N^{-1}(h)
\end{aligned}
$$

---

## Validate

We can validate an **AVL** by validating **Nodes Balance Factor** in $O(n)$

---

## Add

-   Add like **BST**
-   Update node **Height**
-   **Rebalance** tree if required:
    -   If **Balance Factor** greater than **1**:
        -   If **New Key** less than **Left** (Left skewed): `Right rotate`
        -   If **New Key** greater than **Left** (Left arched): `Left-Right rotate`
    -   If **Balance Factor** less than **-1** (Right skewed):
        -   If **New Key** greater than **Right** (Right skewed): `Left rotate`
        -   If **New Key** less than **Right** (Right arched): `Right-Left rotate`

![Add](../../../assets/avl_add.png)

```ts
const add = (head: AVLNode, key: number) => {
    // Recursively find the node
    if (head === null) {
        // Create node
        return new AVLNode(key);
    }
    if (key < head.key) {
        head.left = add(head.left, key);
    }
    if (key > head.key) {
        head.right = add(head.right, key);
    }

    // Update height
    head.height = Math.max(head.left.height, head.right.height) + 1;

    // Rotate if unbalanced
    const balanceFactor = head.left.height - head.left.height;
    if (balanceFactor > 1) {
        if (key < head.left.key) {
            // Left skewed
            return rightRotate(head);
        } else {
            // Left arched
            head.left = leftRotate(head.left);
            return rightRotate(head);
        }
    }
    if (balanceFactor < -1) {
        if (key > head.right.key) {
            // Right skewed
            return leftRotate(head);
        } else {
            // Right arched
            head.right = rightRotate(head.right);
            return leftRotate(head);
        }
    }

    return head;
};
```

---

## Remove

-   Remove like **BST**
-   Update node **Height**
-   **Rebalance** tree if required:
    -   If **Balance Factor** greater than **1**:
        -   If **New Key** less than **Left** (Left skewed): `Right rotate`
        -   If **New Key** greater than **Left** (Left arched): `Left-Right rotate`
    -   If **Balance Factor** less than **-1** (Right skewed):
        -   If **New Key** greater than **Right** (Right skewed): `Left rotate`
        -   If **New Key** less than **Right** (Right arched): `Right-Left rotate`

![Add](../../../assets/avl_add.png)

```ts
const add = (head: AVLNode, key: number) => {
    // Recursively find the node
    if (head === null) {
        // Node not found
        return head;
    }
    if (key < head.key) {
        head.left = remove(head.left, key);
    }
    if (key > head.key) {
        head.right = remove(head.right, key);
    }

    // Node found
    if (key === head.key) {
        // Replace right child
        if (node.left === null) {
            const child = node.right;
            delete node;
            return child;
        }

        // Replace left child
        if (node.right === null) {
            const child = node.left;
            delete node;
            return child;
        }

        // Replace succ or pred
        const pred = min(node.right);
        node.key = pred.key;
        node.right = remove(node.right, pred.key);
    }

    // Update height
    head.height = Math.max(head.left.height, head.right.height) + 1;

    // Rotate if unbalanced
    const balanceFactor = head.left.height - head.left.height;
    if (balanceFactor > 1) {
        if (key < head.left.key) {
            // Left skewed
            return rightRotate(head);
        } else {
            // Left arched
            head.left = leftRotate(head.left);
            return rightRotate(head);
        }
    }
    if (balanceFactor < -1) {
        if (key > head.right.key) {
            // Right skewed
            return leftRotate(head);
        } else {
            // Right arched
            head.right = rightRotate(head.right);
            return leftRotate(head);
        }
    }

    return head;
};
```

---
