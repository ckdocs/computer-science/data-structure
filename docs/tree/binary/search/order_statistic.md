# Order Statistic

**Order Statistic Tree** is type of **BST** that has a **Size** value in each node and optimized the operation of **get(index)** to $O(\log{n})$

-   **Size**: An integer in each node contains the number of **Elements** in **Sub-Trees** plus **1** (Self node)

$$
\begin{aligned}
    & Size = Size(Left) + Size(Right) + 1
\end{aligned}
$$

![Order Statistic Tree](../../../assets/order_statistic_tree.png)

```ts
class Node {
    key: number;
    size: number;

    left: Node;
    right: Node;
    parent: Node;
}
```

Now for finding **n-th** element in **In-Order** traversation we have two ways:

-   **Traverse Keys**: $O(n)$
-   **Search Sizes**: $O(\log{n})$

---
