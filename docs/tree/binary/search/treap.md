# Treap (BST-Heap)

**Treap** or **BST-Heap** is a **BST** that each node has a `priority` (**Binary Heap**)

Each node is a **Tuple** with two main properties:

1. **Key**: BST property
2. **Priority**: Binary Heap property (`Min Heap` or `Max Heap`)

![Treap](../../../assets/treap.png)

```ts
class TreapNode {
    key: number;
    priority: number;

    left: TreapNode;
    right: TreapNode;
}
```

---

## Concepts

There are many important concepts in **Treap**

---

### Tree height

-   **Tip**: The treap can be **Unbalanced** so the height is `not only` in $O(\log{n})$

$$
\begin{aligned}
    & \texttt{Treap Height}: [\log{n}, n]
\end{aligned}
$$

---

### Unique tree

If **Keys** and **Priorities** are unique we can create only one `treap` (treap is unique)

$$
\left\{
\begin{aligned}
    & \texttt{Unique Keys}
    \\
    & \texttt{Unique Priorities}
\end{aligned}
\right\}
\iff
\begin{aligned}
    & \texttt{Unique Treap}
\end{aligned}
$$

---

## Add

-   **Add** like `BST`: $O(n)$
-   **Check** of `New Node` with
-   If **Priority** of **New Node** greater than **Parent**:
    -   **Rotate** it (BST not change)
    -   Recursively call until **Root**

The **Complexity** of this operation is:

$$
\begin{aligned}
    & \texttt{Best Case}: O(1)
    \\
    & \texttt{Average Case}: O(\log{n})
    \\
    & \texttt{Worst Case}: O(n)
\end{aligned}
$$

---

## Create

-   **Sort** items by `priority ascending`: $O(n.\log{n})$
-   **Add** them `one by one` (Tree will be balanced) (No need to rotate): $O(n^2)$

The **Complexity** of this operation is:

$$
\begin{aligned}
    & \texttt{Best Case}: O(n.\log{n})
    \\
    & \texttt{Average Case}: O(n.\log{n})
    \\
    & \texttt{Worst Case}: O(n^2)
\end{aligned}
$$

---
