# Binary Heap

**Binary Heap Tree** or **Binary Heap** is type of **Complete Binary Tree** that satisfies the **Heap Property** and optimized for finding `min`, `max` operations

The properties of this tree are:

1. It's **Complete Binary Tree**
2. Each node is **Smaller** than childrens (`Binary Min Heap`)
3. Each node is **Greater** than childrens (`Binary Max Heap`)

![Binary Heap](../../../assets/binary_heap.png)

-   **Tip**: We can use **Binary Heaps** to implement **Priority Queues**
-   **Tip**: Binary heap is a complete binary tree so we can implement it using **Arrays**:
    ![Binary Heap Array](../../../assets/binary_heap_array.png)

---

## Concepts

There are many important concepts in **Binary Heap**

There are two types of binary heaps:

1. **Binary Min Heap**: Each `node` is **smaller** than `children`
2. **Binary Max Heap**: Each `node` is **greater** than `children`

Now we define operations for **Max Heap**, the other is symmetric

-   **Tip**: By `swapping` the `left sub-tree` and `right sub-tree` in a binary heap:
    -   The **Heap Property** is safe
    -   Tree may not be **Complete**, so it may not be `heap`

---

### Heap size

Number of `keys` existed in **Heap**

---

### Number of trees

Number of possible trees generates with **n** nodes:

1. **Tree Topologies** with **n** keys: There are only **One** `complete binary tree` exists with `n` keys:
    - $$
      \begin{aligned}
          & \texttt{Tree Topologies}: 1
      \end{aligned}
      $$
2. **Key Combinations** with **n** keys: We find the combinations `recursively`
    1. Place the **Max** or **Min** key to **Root**
    2. Select **k** keys for **Left-SubTree**
    3. Other keys for **Right-SubTree**
        - $$
          \begin{aligned}
              & \texttt{Key Combinations}: {n - 1 \choose x} . (\texttt{Recursive left}) . (\texttt{Recursive right})
          \end{aligned}
          $$

---

-   **Example**: What is the number of **Possible Binary Max Heaps** with **10** different keys?

$$
\begin{aligned}
    & {14 \choose 7} ({6 \choose 3} . {2 \choose 1} . {2 \choose 1} ) . ({6 \choose 3} . {2 \choose 1} . {2 \choose 1} )
\end{aligned}
$$

![Heaps](../../../assets/binary_heaps.png)

---

## Validate

For a **Binary Max Heap**:

-   For each **Internal Node** check is **Greater** than `children`

```ts
const validate = (heap: number[]) => {
    for (let i = 1; i <= heap.length / 2; i++) {
        if (heap[i] < heap[2 * i] || heap[i] < heap[2 * i + 1]) {
            return false;
        }
    }

    return true;
};
```

---

## Min, Max

For a **Binary Max Heap**:

-   **Minimum**: Search over `leaf nodes`: $O(\frac{n}{2})$
-   **Maximum**: Return **H[1]**: $O(1)$
-   **K-th Maximum**: Search in levels `2 to k`

```ts
const min = (heap: number[]) => {
    let min = heap[];

    // Search over leaf nodes
    for (let i = heap.length / 2 ; i < heap.length ; i++) {
        if (heap[i] < min) {
            min = heap[i];
        }
    }

    return min;
};

const max = (heap: number[]) => {
    return heap[1];
};

const kthMax = (heap: number[], k: number) => {
    // Find last key index in k-th level
    const lastKeyIndex = (k * (k - 1)) / 2;

    const sorted = Math.sort(heap.slice(2, lastKeyIndex));

    return sorted[k];
};
```

-   **Tip**: There are two ways to find the `k-th maximum`:
    1. **Extract** maximum k times: $O(k \log{n})$
    2. **Search** in levels 2 to k: $O(2^k)$

---

## Heapify (Top to Down)

For a **Binary Max Heap**:

If a key is not in a **Correct Place** (`Less than children`) but the **Left Child** and **Right Child** are heap, this operation will move the key **Down To The Correct Place**

![Heapify](../../../assets/binary_heap_heapify.png)

-   **Left** child is **Heap**
-   **Right** child is **Heap**
-   If **i** is **Less** than children: (`Move down`)
    -   Swap key with **Max** child
    -   Call `fix(k)` (Swapped key)

![Heapify 2](../../../assets/binary_heap_heapify_2.png)

```ts
const heapify = (heap: number[], i: number) => {
    let k = i;

    if (heap[i] < heap[2 * i]) {
        k = 2 * i;
    }
    if (heap[i] < heap[2 * i + 1]) {
        k = 2 * i + 1;
    }

    if (i !== k) {
        swap(heap[i], heap[k]);

        return heapify(k);
    }
};
```

-   **Tip**: In the worst case the **Root Node** will move to **Leafs** so the complexity is $O(\log{n})$

---

## Create

There are two ways of creating a tree:

-   **Bulk Create**: When we access `all the items`:
    -   **Heapify Method**: $O(n)$
        -   Heapify from index **n/2** to **1**
        -   **Proof**: Sum the number of `heapify` operations in each `level`
    -   **Sort Method**: $O(n \log{n})$
        -   **Sort** keys descending
        -   The sorted array is a **Max Binary Heap**
-   **Add Create**: When we input items `one by one`: $O(n \log{n})$
    -   **Add** each item to an empty heap

```ts
const create = (items: number[]) => {
    for (let i = items.length / 2; i >= 0; i--) {
        heapify(items, i);
    }
};
```

---

## Add (Bottom to Up)

For a **Binary Max Heap**:

Is reverse of **Heapify**, will move the **Leaf** to **Root** in $O(\log{n})$

-   Add new node to **Latest** index
-   Compare with **Parent**:
    -   If it was **Greater**: (`Move top`)
        -   Swap **Node** with **Parent**
        -   Call `add(k)` (Swapped key)

![Add](../../../assets/binary_heap_add.png)

```ts
const add = (heap: number[], key: number) => {
    heap.push(key);

    let index = heap.length - 1;
    while (heap[index] > heap[index / 2]) {
        swap(heap[index], heap[index / 2]);
        index = index / 2;
    }
};
```

---

## Extract

-   Swap **Last Key** with **Root**
-   Remove **Last Key**
-   Heapify the **Root**

![Extract](../../../assets/binary_heap_extract.png)

```ts
const extract = (heap: number[]) => {
    swap(heap[1], heap[heap.length - 1]);

    const last = heap.pop();

    heapify(heap, 1);

    return last;
};
```

-   **Tip**: By extracting **n** items from heap we can use **Heap Sort** algorithm:
    1. Create Max Heap with numbers: $O(n)$
    2. Extract Max **n** times: $O(n.\log{n})$

---

## Remove

-   Swap **Last Key** with **Node i**
-   Remove **Last Key**
-   `General Heapify` the **Node i**:
    -   Compare with **Parent** (`Move up`)
    -   Compare with **Children** (`Move down`)

![Remove](../../../assets/binary_heap_remove.png)

```ts
const remove = (heap: number[], i: number) => {
    swap(heap[i], heap[heap.length - 1]);

    const last = heap.pop();

    generalHeapify(heap, i);

    return last;
};
```

---

## Merge

For mergin two **Heaps** with sizes **m** and **n**, we can:

1. **Concatenate** them: $O(1)$
2. Create a new **Heap** by **Bulk Create** algorithm: $O(n+m)$

---
