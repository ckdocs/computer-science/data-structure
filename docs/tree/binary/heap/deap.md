# Deap

**Deap Tree** or **Doubly Heap Tree** or **Doubly Ended Heap Tree** is type of **Complete Binary Tree** that contains a **Binary Min Heap** and **Binary Max Heap** and optimizes both `min` and `max` operations

The properties of this tree are:

1. It's **Complete Binary Tree**
2. Root value is **NULL**
3. **Left sub-tree** is a `Binary Min Heap`
4. **Right sub-tree** is a `Binary Max Heap`
5. Each `left sub-tree` **Key** is less than or equal to `right sub-tree` **Corresponding Key**
    - If a **Key** doesn't have **Corresponding Key** it will compare to **Parent Corresponding Key**

![Deap](../../../assets/deap.jpg)

-   **Tip**: We can use **Deaps** to implement **Priority Dequeue** (`Doubly Ended Priority Queue`)

---

## Corresponding Key

-   If **Key** is in **Left sub-tree**:
    -   Add **Half** of the `nodes of that level` to index
    -   If `index > n` get parent index by `index = index/2`
-   If **Key** is in **Right sub-tree**:
    -   Remove **Half** of the `nodes of that level` to index

$$
\begin{aligned}
    & Corresponding(i) =
    \begin{cases}
        \begin{cases}
            j = i + 2^{(\lfloor \log{i} \rfloor - 1)}
            \\
            if (j > n): j = \lfloor \frac{j}{2} \rfloor
        \end{cases}
        & \texttt{i in Left}
        \\
        j = i - 2^{(\lfloor \log{i} \rfloor - 1)}
        & \texttt{i in Right}
    \end{cases}
\end{aligned}
$$

```ts
const isInLeftSubTree = (i: number) => {
    if (i === 2) {
        return true;
    }
    if (i === 3) {
        return false;
    }

    return isInLeftSubTree(i / 2);
};

const corresponding = (deap: number[], i: number) => {
    if (isInLeftSubTree(i)) {
        const j = i + Math.pow(2, Math.log(i) - 1);

        if (j > deap.length) {
            return j / 2;
        } else {
            return j;
        }
    } else {
        return i - Math.pow(2, Math.log(i) - 1);
    }
};
```

---

## Min, Max

-   **Minimum**: Return **H[2]** (`Root of left sub-tree`)
-   **Maximum**: Return **H[3]** (`Root of right sub-tree`)

```ts
const min = (deap: number[]) => {
    return deap[2];
};

const max = (deap: number[]) => {
    return deap[3];
};
```

---

## Add

-   Add new node to **Latest** index
-   If it was in **Right sub-tree**:
    -   Compare with **Corresponding Key**
    -   If it was **Less**, `swap`
-   If it was in **Left sub-tree**:
    -   Compare with **Corresponding Key**
    -   If it was **Less**, `swap`
-   Now move up like **Binary Heap Add**

```ts
const add = (deap: number[], key: number) => {
    // Add key to last index
    deap.push(key);

    // Move to correct sub-tree
    const index = deap.length - 1;
    const corresponding = corresponding(deap, index);
    if (isInLeftSubTree(key)) {
        if (deap[index] > deap[corresponding]) {
            swap(deap[index], deap[corresponding]);
            index = corresponding;
        }
    } else {
        if (deap[index] < deap[corresponding]) {
            swap(deap[index], deap[corresponding]);
            index = corresponding;
        }
    }

    // Binary Heap add
    while (deap[index] > deap[index / 2]) {
        swap(deap[index], deap[index / 2]);
        index = index / 2;
    }
};
```

---

## Extract

-   Swap **Last Key** with **Min Root** or **Max Root**
-   Remove **Last Key**
-   Heapify the **Min Root** or **Max Root**

```ts
const extractMin = (deap: number[]) => {
    swap(deap[2], deap[deap.length - 1]);

    const last = deap.pop();

    heapify(deap, 2);

    return last;
};

const extractMax = (deap: number[]) => {
    swap(deap[3], deap[deap.length - 1]);

    const last = deap.pop();

    heapify(deap, 3);

    return last;
};
```

---
