# Min-Max Heap

**Min-Max Heap Tree** is type of **Complete Binary Tree** that contains a **Binary Min Heap** at **Even Levels** and **Binary Max Heap** at **Odd Levels** and optimizes both `min` and `max` operations

The properties of this tree are:

1. It's **Complete Binary Tree**
2. **Even** levels have **Min Heap** property
3. **Odd** levels have **Max Heap** property

![Min-Max Heap](../../../assets/min_max_heap.png)

-   **Tip**: We can use **Min-Max Heaps** to implement **Priority Dequeue** (`Doubly Ended Priority Queue`)

---

## Min, Max

-   **Minimum**: Return **H[1]** (`Root`)
-   **Maximum**: Return **Max(H[2], H[3])** (`Nodes 2 or 3`)

```ts
const min = (heap: number[]) => {
    return heap[1];
};

const max = (heap: number[]) => {
    return Math.max(heap[2], heap[3]);
};
```

---

## Add

-   Add new node to **Latest** index
-   Compare with **Parent**:
    -   If parent was in **Min Level**:
        -   If it was **Smaller**: (`swap`)
    -   If parent was in **Max Level**:
        -   If it was **Greater**: (`swap`)
-   If level is **Even** compare with **Min Levels** parents
-   If level is **Odd** compare with **Max Levels** parents

```ts
const add = (heap: number[], key: number) => {
    // Add key to last index
    heap.push(key);

    // Compare with parent
    ...

    // Compare with even levels
    let index = heap.length - 1;
    while (heap[index] > heap[index / 4]) {
        swap(heap[index], heap[index / 4]);
        index = index / 4;
    }

    // Compare with odd levels
    ...
};
```

---

## Extract

-   For removing **Min Root**:
    -   Swap **Last Key** with **Root**
    -   Remove **Last Key**
    -   Heapify the **Min Root** only in **Even Levels**
-   For removing **Max Root**:
    -   Swap **Last Key** with **Root**
    -   Remove **Last Key**
    -   Heapify the **Max Root** only in **Odd Levels**

---
