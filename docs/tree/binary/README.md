# Binary Tree (2-ary)

A type of **Rooted Tree** which each parent node can have `maximum of two` child nodes

-   Each node have **Left Child** and **Right Child**

![Binary Tree](../../assets/binary_tree.png)

---

## Concepts

There are many important concepts in **Binary Tree**

![Nodes Count](../../assets/binary_tree_nodes_count.png)

$$
\begin{aligned}
    & n: \texttt{Number of all nodes}
    \\
    & n_0: \texttt{Number of nodes with 0 child}
    \\
    & n_1: \texttt{Number of nodes with 1 child}
    \\
    & n_2: \texttt{Number of nodes with 2 child}
    \\
    \\
    & n = n_0 + n_1 + n_2
    \\
    & n_0 = n_2 + 1
\end{aligned}
$$

---

### Left, Right

In binary tree **Left** and **Right** childs are different

![Left Child Right Child](../../assets/left_child_right_child.png)

---

## Array Binary Tree

The way of storing a **binary tree** in an **Array List**:

-   We think of a **Complete Binary Tree**
-   Traverse the tree in **Level-Order BFS** and save keys
-   If a node doesn't exist, save **NULL** as it's key

![Array Binary Tree](../../assets/array_binary_tree.jpg)

-   In this way, the **complexity** of operations are:
    -   **Left Child**: $O(1)$
    -   **Right Child**: $O(1)$
    -   **Parent**: $O(1)$
-   $$
    \begin{aligned}
        & left(i) = 2i
        \\
        & right(i) = 2i + 1
        \\
        & parent(i) = \lfloor \frac{i}{2} \rfloor
    \end{aligned}
    $$

---

## Linked Binary Tree

The way of storing a **binary tree** using a **Linked Node**:

```ts
class Node {
    data: number;
    left: Node | null;
    right: Node | null;
}
```

-   In this way, the **complexity** of operations are:
    -   **Left Child**: $O(1)$
    -   **Right Child**: $O(1)$
    -   **Parent**: $O(n)$
-   **Tip**: Number of **NULL Links** in a linked tree with **n** nodes is: $2n - (n - 1)$
-   **Tip**: Does we need **Parent Link** for better performance ?
    -   No, we don't, Binary Tree algorithms required a traverse over tree, so we can access a `child through it's parent`

![Linked Binary Tree](../../assets/linked_binary_tree.png)

---
