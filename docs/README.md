# Intro

Data Structure is a container holds data in a particular format

![Data Structures](./assets/datastructures.png)

---

## Dependencies

1. **Arithmetic**

---

## Data Structure

**Data Structures** are programmatical way of storing **Mathematical Structures** like `Graph`, `Tree`, `List`, `Set`, etc in an application for improving the performance of `Search`, `Insert`, `Delete`, `Update` operations on our data.

---

### Data Type

Every element in a **Data Structure** has a particular type, we create **Data Structures** by combining elements with a particular **Data Types**

---

### Data Object

Every element of data in **Data Structure** called **Data Object**

---

### Data Operation

We create **Data Structures** to optimize performance of operations in our data, these operations are:

1. **Get**: `get(index)`
2. **Update**: `set(index, data)`
3. **Traverse**: `iterate()`
4. **Search**: `contains(data)`, `indexOf(data)`
5. **Insert**: `add(index, data)`, `push(data)`, `unshift(data)`
6. **Delete**: `delete(index)`, `pop()`, `shift()`
7. **Sort**: advanced algorithms
8. **Merge**: advanced algorithms

---
